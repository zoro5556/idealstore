﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;


namespace IdealStore.Converter
{
    public class IsNullOrEmptyLaptopParametrConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BrushConverter ab = new BrushConverter();
            if (value[0] == null || value[0].ToString() == string.Empty)
                return Brushes.Red;
            else
                return (Brush)ab.ConvertFrom("#9E9E9E");//(Color)ColorConverter.ConvertFromString("#9E9E9E"); ;
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
