﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Data.SQLite;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using System.Windows.Shapes;
using DevExpress.Mvvm.Native;
using IdealStore.Model;
using System.Windows.Documents;
using Microsoft.Xaml.Behaviors;
using ToastNotifications.Utilities;
using IdealStore.Model.DBTable;
using Xceed.Wpf.Toolkit.Converters;

namespace IdealStore.Converter
{
    public class IsPCFullColorConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] == null)
                return Brushes.Transparent;
            Border border = value[0] as Border;
            if (border != null)
            {
                Model.ReadyComputer readyComputer = border.DataContext as Model.ReadyComputer;
                foreach (string compName in readyComputer.NameComponents)
                {
                    if (compName == null || compName == string.Empty)
                    {
                        return Brushes.Red;
                    }
                }
                return Brushes.White;
            }
            else
            {//#e0e0e0
                if (value[0].ToString() == "#FFFF0000" || value[0].ToString() == "#ffff0000")
                    return (Color)ColorConverter.ConvertFromString("#ff5131");
                return (Color)ColorConverter.ConvertFromString("#e0e0e0");
            }
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
