﻿using System;
using System.Windows;
using System.Windows.Data;

namespace IdealStore.Converter
{
    public class NullVisibilityConfiguratorConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            /*switch (value[0].ToString())
            {
                case "configuratorPage":*/
                    if (value[3].ToString().Length == 0)
                        switch (value[1].ToString())
                        {
                            case "AddNewCompButton":
                                return value[2].ToString() == string.Empty ? Visibility.Collapsed : Visibility.Visible;
                            case "AddNewCompTypeButton":
                                return value[2].ToString() == string.Empty ? Visibility.Visible : Visibility.Collapsed;
                            case "PriceTextBlock":
                                return Visibility.Collapsed;
                            case "DelCompButton":
                                return Visibility.Collapsed;
                            default:
                                return Visibility.Visible;
                        }
                    else
                    {
                        if (value[1].ToString() == "PriceTextBlock" || value[1].ToString() == "DelCompButton")
                            return Visibility.Visible;
                        else
                            return Visibility.Collapsed;
                    }
                /*case "editPCView":
                    if (value[3].ToString().Length == 0)
                        switch (value[1].ToString())
                        {
                            case "AddNewCompButton":
                                return value[2].ToString() == string.Empty ? Visibility.Collapsed : Visibility.Visible;
                            case "AddNewCompTypeButton":
                                return value[2].ToString() == string.Empty ? Visibility.Visible : Visibility.Collapsed;
                            case "PriceTextBlock":
                                return value[2].ToString() == string.Empty ? Visibility.Collapsed : Visibility.Visible;
                            case "DelCompButton":
                                return Visibility.Collapsed;
                            default:
                                return Visibility.Visible;
                        }
                    else
                    {
                        if (value[1].ToString() == "PriceTextBlock" || value[1].ToString() == "DelCompButton")
                            return Visibility.Visible;
                        else
                            return Visibility.Collapsed;
                    }
                default:
                    return Visibility.Visible;

            }*/
            

        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
