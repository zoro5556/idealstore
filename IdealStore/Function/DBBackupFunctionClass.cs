﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Data.SQLite;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using System.Windows.Data;
using System.Windows.Controls;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using DevExpress.Mvvm.Native;
using System.IO.IsolatedStorage;

namespace IdealStore.Function
{
    public class DBBackupFunctionClass
    {
        public void DBBackupFunction()
        {
            string BackupDB = "BackupDB";
            if (!Directory.Exists(BackupDB))
            {
                Directory.CreateDirectory(BackupDB);
            }
            string[] fileName = Directory.GetFiles("../Debug/BackupDB/", "*")
                    .Select(arg => Path.GetFileName(arg))
                    .ToArray();
            string todayBackup = fileName.Where(arg => arg.Contains(DateTime.Now.ToString("dd-MM-yyyy"))).FirstOrDefault();
            if (todayBackup == null)
            {
                string name = "IdealStoreDB " + DateTime.Now.ToString("dd-MM-yyyy") + ".db";
                using (var source = new SQLiteConnection("Data Source=Resources/IdealStoreDB.db; Version=3;"))
                using (var destination = new SQLiteConnection("Data Source=BackupDB/" + name + "; Version=3;"))
                {
                    source.Open();
                    destination.Open();
                    source.BackupDatabase(destination, "main", "main", -1, null, 0);
                }
            }
        }
    }
}
