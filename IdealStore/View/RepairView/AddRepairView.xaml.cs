﻿using System.Windows;
using System;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Controls;


namespace IdealStore.View.RepairView
{
    public partial class AddRepairView : Window
    {
        public AddRepairView()
        {
            InitializeComponent();
        }

        /*
        private void AcceptAddRepButton_Click(object sender, RoutedEventArgs e)
        {
            bool addError = false;
            if (NameOwnText.Text == string.Empty || NameOwnText.Text == null)
            {
                NameOwnText.BorderBrush = Brushes.Red;
                var ab = new BrushConverter();
                NameOwnText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                addError = true;
            }
            if (NumOwnText.Text == string.Empty || NumOwnText.Text == null || Convert.ToString(NumOwnText.Text) == "___-___-__-__")
            {
                NumOwnText.BorderBrush = Brushes.Red;
                var ab = new BrushConverter();
                NumOwnText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                addError = true;
            }
            if (TypeTechnicsText.Text == string.Empty || TypeTechnicsText.Text == null)
            {
                TypeTechnicsText.BorderBrush = Brushes.Red;
                var ab = new BrushConverter();
                TypeTechnicsText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                addError = true;
            }
            if (DescProbText.Text == string.Empty || DescProbText.Text == null)
            {
                DescProbText.BorderBrush = Brushes.Red;
                var ab = new BrushConverter();
                DescProbText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                addError = true;
            }

            if (addError == false)
            {
                string s = Convert.ToString(NumOwnText.Text);
                for(int i=0; s.Length > i; i++)
                {
                    if (s[i] == Convert.ToChar("_"))
                    {
                        addError = true;
                    }
                }
                if (addError == true)
                {
                    NumOwnText.BorderBrush = Brushes.Red;
                    var ab = new BrushConverter();
                    NumOwnText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                }
                else
                {
                    this.DialogResult = true;
                }
            }
        }*/
        private void NullToText_TextChanged(object sender, RoutedEventArgs e)
        {
            if (sender.ToString() != null)
            {
                var textBox = sender as TextBox;
                var ab = new BrushConverter();
                if (textBox != null)
                {
                    textBox.BorderBrush = (Brush)ab.ConvertFrom("#9E9E9E");
                    textBox.Background = Brushes.Transparent;
                }
                else
                {
                    var comboBox = sender as ComboBox;
                    comboBox.BorderBrush = (Brush)ab.ConvertFrom("#9E9E9E");
                    comboBox.Background = Brushes.Transparent;
                }
            }
        }
        private void PriceText_TextChanged(object sender, RoutedEventArgs e)
        {
            int intCheck;
            bool resultPrice = int.TryParse(PriceText.Text, out intCheck);
            if (resultPrice == false)
            {
                if (resultPrice == false)
                {
                    PriceText.BorderBrush = Brushes.Red;
                    var ab = new BrushConverter();
                    PriceText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                }
            }
        }
        private void PriceOwnText_TextChanged(object sender, RoutedEventArgs e)
        {
            int intCheck;
            bool resultPriceOwn = int.TryParse(PriceOwnText.Text, out intCheck);
            if (resultPriceOwn == false)
            {
                if (resultPriceOwn == false)
                {
                    PriceOwnText.BorderBrush = Brushes.Red;
                    var ab = new BrushConverter();
                    PriceOwnText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                }
            } else
            {

                var ab = new BrushConverter();
                PriceOwnText.BorderBrush = (Brush)ab.ConvertFrom("#9E9E9E");
                PriceOwnText.Background = Brushes.Transparent;
            }
        }
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.D0:
                    e.Handled = false;
                    break;
                case Key.D1:
                    e.Handled = false;
                    break;
                case Key.D2:
                    e.Handled = false;
                    break;
                case Key.D3:
                    e.Handled = false;
                    break;
                case Key.D4:
                    e.Handled = false;
                    break;
                case Key.D5:
                    e.Handled = false;
                    break;
                case Key.D6:
                    e.Handled = false;
                    break;
                case Key.D7:
                    e.Handled = false;
                    break;
                case Key.D8:
                    e.Handled = false;
                    break;
                case Key.D9:
                    e.Handled = false;
                    break;
                default:
                    e.Handled = true;
                    break;
            }
        }

    }
}
