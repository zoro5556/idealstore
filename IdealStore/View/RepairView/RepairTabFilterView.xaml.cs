﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IdealStore.View.RepairView
{
    /// <summary>
    /// Interaction logic for RepairTabFilterView.xaml
    /// </summary>
    public partial class RepairTabFilterView : Window
    {
        public RepairTabFilterView()
        {
            InitializeComponent();
        }
        private void AcceptFilterRepButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
        private void CancelFilterRepButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
