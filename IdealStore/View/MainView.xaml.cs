﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Data.SQLite;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using System.Windows.Shapes;
using System.Threading.Tasks;
using System.Threading;


namespace IdealStore.View
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        
        public MainView()
        {
            
            InitializeComponent();
            this.DataContext = new ViewModel.MainViewModel(this);
            /*frameRepair.NavigationService.Navigate(new Uri("/View/Tabs/RepairTab.xaml", UriKind.Relative));
            frameGood.NavigationService.Navigate(new Uri("/View/Tabs/GoodTab.xaml", UriKind.Relative));
            frameConfigurator.NavigationService.Navigate(new Uri("/View/Tabs/ConfiguratorTab.xaml", UriKind.Relative));
            framePC.NavigationService.Navigate(new Uri("/View/Tabs/PCTab.xaml", UriKind.Relative));
            frameUsed.NavigationService.Navigate(new Uri("/View/Tabs/UsedTab.xaml", UriKind.Relative));
            frameIncome.NavigationService.Navigate(new Uri("/View/Tabs/IncomeTab.xaml", UriKind.Relative));
            frameBasket.NavigationService.Navigate(new Uri("/View/Tabs/BasketTab.xaml", UriKind.Relative));*/
        }
    }
}
