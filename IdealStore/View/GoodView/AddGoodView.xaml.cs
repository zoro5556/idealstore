﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Controls;

namespace IdealStore.View.GoodView
{
    public partial class AddGoodView : Window
    {
        public AddGoodView()
        {            
            InitializeComponent();
        }
        private void NullToText_TextChanged(object sender, RoutedEventArgs e)
        {
           /* if (sender.ToString() != null)
            {
                var textBox = sender as TextBox;
                var ab = new BrushConverter();
                if (textBox != null)
                {
                    if (textBox.Name == "PriceText")
                    {
                        int intCheck;
                        bool resultPriceOwn = int.TryParse(textBox.Text, out intCheck);
                        if (resultPriceOwn == false)
                        {
                            textBox.BorderBrush = Brushes.Red;
                            textBox.Background = (Brush)ab.ConvertFrom("#FFDADA");
                            return;
                        }
                    }
                    textBox.BorderBrush = (Brush)ab.ConvertFrom("#9E9E9E");
                    textBox.Background = Brushes.Transparent;
                }
                else
                {
                    var comboBox = sender as ComboBox;
                    comboBox.BorderBrush = (Brush)ab.ConvertFrom("#9E9E9E");
                    comboBox.Background = Brushes.Transparent;
                }
            }*/
        }
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.D0:
                    e.Handled = false;
                    break;
                case Key.D1:
                    e.Handled = false;
                    break;
                case Key.D2:
                    e.Handled = false;
                    break;
                case Key.D3:
                    e.Handled = false;
                    break;
                case Key.D4:
                    e.Handled = false;
                    break;
                case Key.D5:
                    e.Handled = false;
                    break;
                case Key.D6:
                    e.Handled = false;
                    break;
                case Key.D7:
                    e.Handled = false;
                    break;
                case Key.D8:
                    e.Handled = false;
                    break;
                case Key.D9:
                    e.Handled = false;
                    break;
                default:
                    e.Handled = true;
                    break;
            }
        }
        private void CancelAddGoodButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
