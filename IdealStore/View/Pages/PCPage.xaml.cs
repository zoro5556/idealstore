﻿using System.Windows.Controls;

namespace IdealStore.View.Pages
{
    /// <summary>
    /// Interaction logic for PCTab.xaml
    /// </summary>
    public partial class PCPage : Page
    {
        public PCPage()
        {
            InitializeComponent();
            this.DataContext = new ViewModel.PCViewModel();
        }
    }
}
