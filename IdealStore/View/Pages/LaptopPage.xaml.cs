﻿using System.Windows.Controls;

namespace IdealStore.View.Pages
{
    public partial class LaptopPage : Page
    {
        public LaptopPage()
        {
            InitializeComponent();
            this.DataContext = new ViewModel.LaptopViewModel();
        }
    }
}
