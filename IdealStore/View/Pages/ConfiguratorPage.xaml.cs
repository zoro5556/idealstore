﻿using System.Windows.Controls;

namespace IdealStore.View.Pages
{
    /// <summary>
    /// Interaction logic for ConfiguratorTab.xaml
    /// </summary>
    public partial class ConfiguratorPage : Page
    {
        public ConfiguratorPage()
        {
            InitializeComponent();
            this.DataContext = new ViewModel.ConfiguratorViewModel();
        }
    }
}
