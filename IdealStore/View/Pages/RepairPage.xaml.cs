﻿using System.Windows.Controls;
using System.Windows;


namespace IdealStore.View.Pages
{
    /// <summary>
    /// Interaction logic for RepairTab.xaml
    /// </summary>
    public partial class RepairPage : Page
    {
        public RepairPage()
        {
            InitializeComponent();
            this.DataContext = new ViewModel.RepairViewModel();
        }

    }
}
