﻿using System.Windows.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Data.SQLite;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using System.Windows.Data;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;

namespace IdealStore.View.Pages
{
    /// <summary>
    /// Interaction logic for GoodTab.xaml
    /// </summary>
    public partial class GoodPage : Page
    {
        public GoodPage()
        {
            InitializeComponent();
            this.DataContext = new ViewModel.GoodViewModel();
        }
    }
}
