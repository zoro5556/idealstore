﻿using System.Windows;

namespace IdealStore.View
{
    /// <summary>
    /// Interaction logic for AuthorizationView.xaml
    /// </summary>
    public partial class AuthorizationView : Window
    {
        public AuthorizationView()
        {
            InitializeComponent();
        }

        private void ButtonEntry_Click(object sender, RoutedEventArgs e)
        {
            MainView mV = new MainView();
            mV.Show();
            this.Close();
        }

        private void ButtonReg_Click(object sender, RoutedEventArgs e)
        {

        }
        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            txtHintPassword.Visibility = Visibility.Visible;
            if (txtPassword.Password.Length > 0)
            {
                txtHintPassword.Visibility = Visibility.Hidden;
            }
        }
        private void txtLogin_TextChanged(object sender, RoutedEventArgs e)
        {
            txtHintLogin.Visibility = Visibility.Visible;
            if (txtLogin.Text.Length > 0)
            {
                txtHintLogin.Visibility = Visibility.Hidden;
            }
        }
    }
}
