﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IdealStore.View
{
    /// <summary>
    /// Interaction logic for MessageBox.xaml
    /// </summary>
    public partial class MessageBox : Window
    {
        public MessageBox()
        {
            InitializeComponent();
        }
        void AddButtons(MessageBoxButton buttons, string firstbut, string secondbut, string thirdbut)
        {
            switch (buttons)
            {
                case MessageBoxButton.OK:
                    AddButton(firstbut, MessageBoxResult.OK);
                    break;
                case MessageBoxButton.OKCancel:
                    AddButton(firstbut, MessageBoxResult.OK);
                    AddButton(secondbut, MessageBoxResult.Cancel, isCancel: true);
                    break;
                case MessageBoxButton.YesNo:
                    AddButton(firstbut, MessageBoxResult.Yes);
                    AddButton(secondbut, MessageBoxResult.No);
                    break;
                case MessageBoxButton.YesNoCancel:
                    AddButton(firstbut, MessageBoxResult.Yes);
                    AddButton(secondbut, MessageBoxResult.No);
                    AddButton(thirdbut, MessageBoxResult.Cancel, isCancel: true);
                    break;
                default:
                    throw new ArgumentException("Unknown button value", "buttons");
            }
        }

        void AddButton(string text, MessageBoxResult result, bool isCancel = false)
        {
            var button = new Button() { Content = text, IsCancel = isCancel };
            button.Click += (o, args) => { Result = result; DialogResult = true; };
            if (result == MessageBoxResult.Yes || result == MessageBoxResult.No )
            {
                ButtonContainer.Children.Add(button);
            }
            if (result == MessageBoxResult.OK)
            {
                button.IsDefault = true;
                SecondButtonContainer.Children.Add(button);
            }
            if (result == MessageBoxResult.Cancel)
            {
                button.IsCancel = true;
                ThirdButtonContainer.Children.Add(button);
            }

        }

        MessageBoxResult Result = MessageBoxResult.None;

        public static MessageBoxResult Show(string message, string caption,
                                            MessageBoxButton buttons, string firstbut, string secondbut, string thirdbut)
        {
            var dialog = new MessageBox() { Title = caption };
            dialog.MessageContainer.Text = message;
            dialog.AddButtons(buttons, firstbut, secondbut,thirdbut);
            dialog.ShowDialog();
            return dialog.Result;
        }
    }
}
