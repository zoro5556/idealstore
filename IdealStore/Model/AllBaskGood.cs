﻿using Prism.Mvvm;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;

namespace IdealStore
{
    public class AllBaskGood : INotifyPropertyChanged
    {
        private int idGood;
        private string name;
        private int salePrice;
        private int quantity;
        private int maxQuantity;


        public int Id { get; set; }
        public int IdGood
        {
            get { return idGood; }
            set
            {
                idGood = value;
                OnPropertyChanged("IdGood");
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public int SalePrice
        {
            get { return salePrice; }
            set
            {
                salePrice = value;
                OnPropertyChanged("SalePrice");
            }
        }
        public int MaxQuantity
        {
            get { return maxQuantity; }
            set
            {
                maxQuantity = value;
                OnPropertyChanged("MaxQuantity");
            }
        }
        public int Quantity
        {
            get { return quantity; }
            set
            {
                if (value <= maxQuantity)
                {
                    if (value == 0)
                        value = 1;
                    quantity = value;
                }
                else quantity = maxQuantity;
                OnPropertyChanged("Quantity");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
