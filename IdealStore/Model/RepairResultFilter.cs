﻿using Prism.Mvvm;

namespace IdealStore.Model
{
    public class RepairResultFilter : BindableBase
    {
        private string date;
        private string descriptionProblem;
        private string nameOwner;
        private string numberOwner;
        private int price;
        private int priceOwner;
        private string dateSecond;
        private string comment;
        private int deleted;
        private int externalRep;
        string name;
        string typeTechnics;
        private string sN;

        public int Id { get; set; }
        public string TypeTechnics
        {
            get { return typeTechnics; }
            set
            {
                typeTechnics = value;
                RaisePropertyChanged("TypeTechnics");
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged("Name");
            }
        }
        public string SN
        {
            get { return sN; }
            set
            {
                sN = value;
                RaisePropertyChanged("SN");
            }
        }

        public string Date
        {
            get { return date; }
            set
            {
                date = value;
                RaisePropertyChanged("Date");
            }
        }
        public string DescriptionProblem
        {
            get { return descriptionProblem; }
            set
            {
                descriptionProblem = value;
                RaisePropertyChanged("DescriptionProblem");
            }
        }
        public string NameOwner
        {
            get { return nameOwner; }
            set
            {
                nameOwner = value;
                RaisePropertyChanged("NameOwner");
            }
        }
        public string NumberOwner
        {
            get { return numberOwner; }
            set
            {
                numberOwner = value;
                RaisePropertyChanged("NumberOwner");
            }
        }
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                RaisePropertyChanged("PriceRepair");
            }
        }
        public int PriceOwner
        {
            get { return priceOwner; }
            set
            {
                priceOwner = value;
                RaisePropertyChanged("PriceOwner");
            }
        }
        public string DateSecond
        {
            get { return dateSecond; }
            set
            {
                dateSecond = value;
                RaisePropertyChanged("DateSecond");
            }
        }
        public string Comment
        {
            get { return comment; }
            set
            {
                comment = value;
                RaisePropertyChanged("Comment");
            }
        }
        public int Deleted
        {
            get { return deleted; }
            set
            {
                deleted = value;
                RaisePropertyChanged("Delete");
            }
        }
        public int ExternalRep
        {
            get { return externalRep; }
            set
            {
                externalRep = value;
                RaisePropertyChanged("ExternalRep");
            }
        }
    }
}
