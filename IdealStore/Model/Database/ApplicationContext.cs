﻿using System.Data.Entity;

namespace IdealStore
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("DefaultConnection")
        {

        }
        public DbSet<Model.DBTable.Good> Goods { get; set; }
        public DbSet<Model.DBTable.Repair> Repairs { get; set; }
        public DbSet<Model.DBTable.BasketGood> BasketGoods { get; set; }
        public DbSet<Model.DBTable.Transaction> Transactions { get; set; }
        public DbSet<Model.DBTable.Sale> Sales { get; set; }
        public DbSet<Model.DBTable.SerialNumber> SerialNumbers { get; set; }
        public DbSet<Model.DBTable.TypeGood> TypeGoods { get; set; }
        public DbSet<Model.DBTable.Computer> Computers { get; set; }
        public DbSet<Model.DBTable.Component> Components { get; set; }
        public DbSet<Model.DBTable.ConfiguratorComponent> ConfiguratorComponents { get; set; }
        public DbSet<Model.DBTable.TypeTechnicsRepairs> TypeTechnicsRepairs { get; set; }
    }
}