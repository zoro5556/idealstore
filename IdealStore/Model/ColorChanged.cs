﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;

namespace IdealStore.Model
{
    public class ColorChanged : INotifyPropertyChanged
    {
        private int idColor;
        private Brush nameColor;

        public int IdColor
        {
            get { return idColor; }
            set
            {
                idColor = value;
                OnPropertyChanged("IdColor");
            }
        }
        public Brush NameColor
        {
            get { return nameColor; }
            set
            {
                nameColor = value;
                OnPropertyChanged("NameColor");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
