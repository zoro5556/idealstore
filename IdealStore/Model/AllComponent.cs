﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace IdealStore.Model
{
    public class AllComponent : INotifyPropertyChanged
    {
        private string name;
        private int idGood;
        private int idComputer;
        private string compType;
        private int price;
        private string sn;

        public int Id { get; set; }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public string CompType
        {
            get { return compType; }
            set
            {
                compType = value;
                OnPropertyChanged("CompType");
            }
        }
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                OnPropertyChanged("Price");
            }
        }
        public int IdGood
        {
            get { return idGood; }
            set
            {
                idGood = value;
                OnPropertyChanged("IdGood");
            }
        }
        public int IdComputer
        {
            get { return idComputer; }
            set
            {
                idComputer = value;
                OnPropertyChanged("IdComputer");
            }
        }
        public string SN
        {
            get { return sn; }
            set
            {
                sn = value;
                OnPropertyChanged("SN");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
