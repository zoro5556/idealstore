﻿using Prism.Mvvm;

namespace IdealStore.Model
{
    public class AllLaptop : BindableBase
    {
        private string name;
        int price;
        private int salePrice;
        private string barcode;
        private string description;
        private string used;
        private string sN;
        int guarante;


        public int Id { get; set; }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged("Name");
            }
        }
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                RaisePropertyChanged("Price");
            }
        }
        public int SalePrice
        {
            get { return salePrice; }
            set
            {
                salePrice = value;
                RaisePropertyChanged("SalePrice");
            }
        }
        public string Barcode
        {
            get { return barcode; }
            set
            {
                barcode = value;
                RaisePropertyChanged("Barcode");
            }
        }
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                RaisePropertyChanged("Description");
            }
        }
        public string Used
        {
            get { return used; }
            set
            {
                used = value;
                RaisePropertyChanged("Used");
            }
        }
        public string SN
        {
            get { return sN; }
            set
            {
                sN = value;
                RaisePropertyChanged("SN");
            }
        }
        public int Guarante
        {
            get { return guarante; }
            set
            {
                guarante = value;
                RaisePropertyChanged("Guarante");
            }
        }
    }
}
