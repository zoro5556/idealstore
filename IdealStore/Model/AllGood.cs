﻿using Prism.Mvvm;

namespace IdealStore.Model
{
    public class AllGood : BindableBase
    {
        private string kode;
        private string name;
        private int price;
        private int salePrice;
        private int quantity;
        private int guarante;
        private string goodType;
        private string barcode;
        private string description;
        private int used;
        private int deleted;
        private int sN;


        public int Id { get; set; }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged("Name");
            }
        }
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                RaisePropertyChanged("Price");
            }
        }
        public int SalePrice
        {
            get { return salePrice; }
            set
            {
                salePrice = value;
                RaisePropertyChanged("SalePrice");
            }
        }
        public int Quantity
        {
            get { return quantity; }
            set
            {
                if (value >= 0)
                    quantity = value;
                else
                    quantity = 0;
                RaisePropertyChanged("Quantity");
            }
        }
        public int Guarante
        {
            get { return guarante; }
            set
            {
                if (value >= 0)
                    guarante = value;
                RaisePropertyChanged("Guarante");
            }
        }
        public string Kode
        {
            get { return kode; }
            set
            {
                kode = value;
                RaisePropertyChanged("Kode");
            }
        }
        public string GoodType
        {
            get { return goodType; }
            set
            {
                goodType = value;
                RaisePropertyChanged("GoodType");
            }
        }
        public string Barcode
        {
            get { return barcode; }
            set
            {
                barcode = value;
                RaisePropertyChanged("Barcode");
            }
        }
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                RaisePropertyChanged("Description");
            }
        }
        public int Used
        {
            get { return used; }
            set
            {
                used = value;
                RaisePropertyChanged("Used");
            }
        }
        public int Deleted
        {
            get { return deleted; }
            set
            {
                deleted = value;
                RaisePropertyChanged("Deleted");
            }
        }
        public int SN
        {
            get { return sN; }
            set
            {
                sN = value;
                RaisePropertyChanged("SN");
            }
        }
    }
}
