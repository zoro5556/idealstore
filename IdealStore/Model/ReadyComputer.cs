﻿using Prism.Mvvm;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

namespace IdealStore.Model
{
    public class ReadyComputer : INotifyPropertyChanged
    {
        string pcName;
        int price;
        private int idComputer;
        private ObservableCollection<string> nameComponents;
        string used;

        public int Id { get; set; }

        public int IdComputer
        {
            get { return idComputer; }
            set
            {
                idComputer = value;
                OnPropertyChanged("IdComputer");
            }
        }
        public string PCName
        {
            get { return pcName; }
            set
            {
                pcName = value;
                OnPropertyChanged("PCName");
            }
        }
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                OnPropertyChanged("Price");
            }
        }
        public ObservableCollection<string> NameComponents
        {
            get { return nameComponents; }
            set
            {
                nameComponents = value;
                OnPropertyChanged("NameComponent");
            }
        }
        public string Used
        {
            get { return used; }
            set
            {
                used = value;
                OnPropertyChanged("Used");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
