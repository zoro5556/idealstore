﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace IdealStore.Model.DBTable
{
    public class Component : INotifyPropertyChanged
    {
        private int idGood;
        private int idType;
        private int idSN;
        private int price;
        private int idComputer;

        public int Id { get; set; }

        public int IdGood
        {
            get { return idGood; }
            set
            {
                idGood = value;
                OnPropertyChanged("IdGood");
            }
        }
        public int IdType
        {
            get { return idType; }
            set
            {
                idType = value;
                OnPropertyChanged("IdType");
            }
        }
        public int IdSN
        {
            get { return idSN; }
            set
            {
                idSN = value;
                OnPropertyChanged("IdSN");
            }
        }
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                OnPropertyChanged("Price");
            }
        }
        public int IdComputer
        {
            get { return idComputer; }
            set
            {
                idComputer = value;
                OnPropertyChanged("IdComputer");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
