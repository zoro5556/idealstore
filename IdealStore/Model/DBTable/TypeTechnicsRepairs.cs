﻿using System.ComponentModel.DataAnnotations;
using Prism.Mvvm;
using System.Collections.Generic;

namespace IdealStore.Model.DBTable
{
    public class TypeTechnicsRepairs : BindableBase
    {
        private string type;

        [Key]
        public int Id { get; set; }

        public string Type
        {
            get { return type; }
            set
            {
                type = value;
                RaisePropertyChanged("Type");
            }
        }
    }
}