﻿using System.ComponentModel.DataAnnotations;
using Prism.Mvvm;
using System;

namespace IdealStore.Model.DBTable
{
    public class BasketGood : BindableBase
    {
        private int idGood;
        private int quantity;
        int salePrice;

        public int Id { get; set; }
        public int IdGood
        {
            get { return idGood; }
            set
            {
                idGood = value;
                RaisePropertyChanged("IdGood");
            }
        }
        public int Quantity
        {
            get { return quantity; }
            set
            {
                quantity = value;
                RaisePropertyChanged("Quantity");
            }
        }
        public int SalePrice
        {
            get { return salePrice; }
            set
            {
                salePrice = value;
                RaisePropertyChanged("SalePrice");
            }
        }
    }
}
