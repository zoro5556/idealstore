﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;
using System.Windows.Data;
using System.Windows.Media;

namespace IdealStore.Model.DBTable
{
    public class SerialNumber : INotifyPropertyChanged
    {
        private int idGood;
        private string sN;
        private int idTransaction;
        private int idComputer;

        public int Id { get; set; }

        public int IdGood
        {
            get { return idGood; }
            set
            {
                idGood = value;
                OnPropertyChanged("IdGood");
            }
        }
        public string SN
        {
            get { return sN; }
            set
            {
                sN = value;
                OnPropertyChanged("SN");
            }
        }
        public int IdTransaction
        {
            get { return idTransaction; }
            set
            {
                idTransaction = value;
                OnPropertyChanged("SoldOut");
            }
        }
        public int IdComputer
        {
            get { return idComputer; }
            set
            {
                idComputer = value;
                OnPropertyChanged("IdComputer");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
