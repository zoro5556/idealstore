﻿using Prism.Mvvm;

namespace IdealStore.Model.DBTable
{
    public class TypeGood : BindableBase
    {
        private string goodType;

        public int Id { get; set; }

        public string GoodType
        {
            get { return goodType; }
            set
            {
                goodType = value;
                RaisePropertyChanged("GoodType");
            }
        }
    }
}
