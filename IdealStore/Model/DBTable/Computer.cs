﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace IdealStore.Model.DBTable
{
    public class Computer : INotifyPropertyChanged
    {
        private int totalPCPrice;
        private int used;
        private string saleDate;
        string pcName;
        int sale;

        public int Id { get; set; }

        public int TotalPcPrice
        {
            get { return totalPCPrice; }
            set
            {
                totalPCPrice = value;
                OnPropertyChanged("TotalPcPrice");
            }
        }
        public int Used
        {
            get { return used; }
            set
            {
                used = value;
                OnPropertyChanged("Used");
            }
        }
        public string SaleDate
        {
            get { return saleDate; }
            set
            {
                saleDate = value;
                OnPropertyChanged("SaleDate");
            }
        }
        public string PCName
        {
            get { return pcName; }
            set
            {
                pcName = value;
                OnPropertyChanged("PCName");
            }
        }
        public int Sale
        {
            get { return sale; }
            set
            {
                sale = value;
                OnPropertyChanged("Sale");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
