﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace IdealStore.Model.DBTable
{
    public class ConfiguratorComponent : INotifyPropertyChanged
    {
        int idGood;
        string sn;

        public int Id { get; set; }

        public int IdGood
        {
            get { return idGood; }
            set
            {
                idGood = value;
                OnPropertyChanged("IdGood");
            }
        }
        public string SN
        {
            get { return sn; }
            set
            {
                sn = value;
                OnPropertyChanged("SN");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
