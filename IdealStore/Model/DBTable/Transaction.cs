﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace IdealStore.Model.DBTable
{
    public class Transaction : INotifyPropertyChanged
    {
        private string date;
        private int sum;
        private int cash;

        public int Id { get; set; }
        public string Date
        {
            get { return date; }
            set
            {
                date = value;
                OnPropertyChanged("Date");
            }
        }
        public int Sum
        {
            get { return sum; }
            set
            {
                sum = value;
                OnPropertyChanged("Sum");
            }
        }
        public int Cash
        {
            get { return cash; }
            set
            {
                cash = value;
                OnPropertyChanged("Cash");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
