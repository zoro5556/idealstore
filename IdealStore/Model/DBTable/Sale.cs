﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace IdealStore.Model.DBTable
{
    public class Sale : INotifyPropertyChanged
    {
        private int idGood;
        private int quantity;
        private int idTransaction;
        private int salePrice;
        int returned;

        public int Id { get; set; }

        public int IdGood
        {
            get { return idGood; }
            set
            {
                idGood = value;
                OnPropertyChanged("IdGood");
            }
        }
        public int Quantity
        {
            get { return quantity; }
            set
            {
                quantity = value;
                OnPropertyChanged("Quantity");
            }
        }
        public int IdTransaction
        {
            get { return idTransaction; }
            set
            {
                idTransaction = value;
                OnPropertyChanged("IdTransaction");
            }
        }
        public int SalePrice
        {
            get { return salePrice; }
            set
            {
                salePrice = value;
                OnPropertyChanged("SalePrice");
            }
        }
        public int Returned
        {
            get { return returned; }
            set
            {
                returned = value;
                OnPropertyChanged("Returned");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
