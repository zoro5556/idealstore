﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Data.SQLite;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using System.Windows.Data;
using System.Windows.Controls;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using System.Windows.Shapes;
using DevExpress.Mvvm.Native;


namespace IdealStore.ViewModel
{
    public class GoodViewModel : DependencyObject, INotifyPropertyChanged
    {
        ApplicationContext db;
        RelayCommand goodReturnCommand;
        RelayCommand inBasketGoodCommand;
        RelayCommand goodAddCommand;
        RelayCommand goodEditCommand;
        RelayCommand goodDeleteCommand;
        RelayCommand goodSearchCommand;
        RelayCommand goodPlusQuanCommand;
        RelayCommand goodPlusGuaranteCommand;
        RelayCommand goodMinusQuanCommand;
        RelayCommand goodMinusGuaranteCommand;
        RelayCommand clearKodeTextCommand;
        RelayCommand acceptButtonCommand;
        RelayCommand sNCheckBoxChangeCommand;
        RelayCommand acceptAddGoodButtonCommand;
        RelayCommand acceptReturnSaleCommand;
        RelayCommand dropDownClosed;
        RelayCommand goodFilterCommand;
        RelayCommand filterGoodAcceptCommand;
        RelayCommand clearGoodTypeFilter;
        IEnumerable<Model.DBTable.Good> goods;
        IEnumerable<Model.DBTable.BasketGood> basketGoods;
        IEnumerable<Model.DBTable.SerialNumber> serialNumbers;
        IEnumerable<Model.DBTable.TypeGood> typeGoods;
        List<Model.DBTable.Transaction> transactions;
        List<Model.AllGood> allGoods;
        private Model.AllGood allGood;
        private string kodeStr;
        private bool nextGood;
        private string dateReturnText;
        private string snReturnText;
        private Model.DBTable.Good returnedGood;
        private Brush returnComboBoxBorder;
        private bool sNCheckBox_IsChecked;
        private bool missingCheckBox_IsChecked;
        private bool inStockCheckBox_IsChecked;
        private string goodTypeFilterText;

        public string GoodTypeFilterText
        {
            get { return goodTypeFilterText; }
            set
            {
                goodTypeFilterText = value;
                OnPropertyChanged("GoodTypeFilterText");
            }
        }
        public bool InStockCheckBox_IsChecked
        {
            get { return inStockCheckBox_IsChecked; }
            set
            {
                inStockCheckBox_IsChecked = value;
                OnPropertyChanged("InStockCheckBox_IsChecked");
            }
        }
        public bool MissingCheckBox_IsChecked
        {
            get { return missingCheckBox_IsChecked; }
            set
            {
                missingCheckBox_IsChecked = value;
                OnPropertyChanged("MissingCheckBox_IsChecked");
            }
        }
        public bool SNCheckBox_IsChecked
        {
            get { return sNCheckBox_IsChecked; }
            set
            {
                sNCheckBox_IsChecked = value;
                OnPropertyChanged("SNCheckBox_IsChecked");
            }
        }
        public string DateReturnText
        {
            get { return dateReturnText; }
            set
            {
                dateReturnText = value;
                OnPropertyChanged("DateReturnText");
            }
        }
        public string SNReturnText
        {
            get { return snReturnText; }
            set
            {
                snReturnText = value;
                OnPropertyChanged("SNReturnText");
            }
        }
        public Brush ReturnComboBoxBorder
        {
            get { return returnComboBoxBorder; }
            set
            {
                returnComboBoxBorder = value;
                OnPropertyChanged("ReturnComboBoxBorder");
            }
        }
        public int QuanSimGood { get; set; }
        public string KodeStr
        {
            get { return kodeStr; }
            set
            {
                kodeStr = value;
                OnPropertyChanged("KodeStr");
            }
        }
        public Model.AllGood AllGood
        {
            get { return allGood; }
            set
            {
                allGood = value;
                OnPropertyChanged("AllGood");
            }
        }
        public IEnumerable<Model.DBTable.BasketGood> BasketGoods
        {
            get { return basketGoods; }
            set
            {
                basketGoods = value;
                OnPropertyChanged("BasketGoods");
            }
        }
        public IEnumerable<Model.DBTable.Good> Goods
        {
            get { return goods; }
            set
            {
                goods = value;
                OnPropertyChanged("Goods");
            }
        }
        public IEnumerable<Model.DBTable.SerialNumber> SerialNumbers
        {
            get { return serialNumbers; }
            set
            {
                serialNumbers = value;
                OnPropertyChanged("SerialNumbers");
            }
        }
        public IEnumerable<Model.DBTable.TypeGood> TypeGoods
        {
            get { return typeGoods; }
            set
            {
                typeGoods = value;
                OnPropertyChanged("TypeGoods");
            }
        }
        public List<Model.DBTable.Transaction> Transactions
        {
            get { return transactions; }
            set
            {
                transactions = value;
                OnPropertyChanged("Transactions");
            }
        }
        public List<Model.AllGood> AllGoods
        {
            get { return allGoods; }
            set
            {
                allGoods = value;
                OnPropertyChanged("AllGoods");
            }
        }

        public static readonly DependencyProperty goodProperty = DependencyProperty.Register("goodSearchText", typeof(string), typeof(View.Pages.GoodPage));
        public string goodSearchText
        {
            get { return (string)GetValue(goodProperty); }
            set { SetValue(goodProperty, value); }
        }
        //-----------------------------------------------------

        public GoodViewModel()
        {
            InStockCheckBox_IsChecked = true;
            MissingCheckBox_IsChecked = false;
            AllGoodsFunction();
            
        }


        //-Function-----------------------------------------------
        public void AllGoodsFunction()
        {
            db = new ApplicationContext();
            AllGoods = (from g in db.Goods
                        join t in db.TypeGoods
                        on g.GoodType equals t.Id
                        where g.Deleted == 0
                        where g.GoodType != 29 //тип ноутбук 
                        orderby g.Id descending
                        select new Model.AllGood
                        {
                            Id = g.Id,
                            Kode = g.Kode,
                            Name = g.Name,
                            Price = g.Price,
                            SalePrice = g.SalePrice,
                            Quantity = g.Quantity,
                            Guarante = g.Guarante,
                            GoodType = t.GoodType,
                            Barcode = g.Barcode,
                            Description = g.Description,
                            Used = g.Used,
                            Deleted = g.Deleted,
                            SN = g.SN
                        }).ToList();
            if(InStockCheckBox_IsChecked == false)
                AllGoods = AllGoods.Where(arg => arg.Quantity == 0).ToList();
            if (MissingCheckBox_IsChecked == false)
                AllGoods = AllGoods.Where(arg => arg.Quantity != 0).ToList();
            if (GoodTypeFilterText != null && GoodTypeFilterText.Length > 0)
                AllGoods = AllGoods.Where(arg => arg.GoodType == GoodTypeFilterText).ToList();

            if (goodSearchText != null)
            {
                string searchText = goodSearchText.ToLower();
                if (searchText != null || searchText != string.Empty)
                    AllGoods = AllGoods.Where(arg => arg.Name.ToLower().Contains(searchText)
                    || arg.Barcode.ToLower().Contains(searchText)).ToList();
            }
        }
        public int TypeIdCheckFunction(string type)
        {
            char[] typeChar = type.ToCharArray();
            for (int i = 0; i < typeChar.Length; i++)
            {
                typeChar[i] = char.ToLower(typeChar[i]);
            }
            type = new string(typeChar);
            int IdSelect = 0;
            foreach (var dbTypeCheck in TypeGoods)
            {
                char[] dbTypeChar = dbTypeCheck.GoodType.ToCharArray();
                for (int i = 0; i < dbTypeChar.Length; i++)
                {
                    dbTypeChar[i] = char.ToLower(dbTypeChar[i]);
                }
                string LowerDBCheck = new string(dbTypeChar);
                if (LowerDBCheck == type)
                {
                    IdSelect = dbTypeCheck.Id;
                }
            }
            if (IdSelect != 0)
            {
                return IdSelect;
            }
            else
                if (IdSelect == 0)
            {
                typeChar[0] = char.ToUpper(typeChar[0]);
                Model.DBTable.TypeGood typeGood = new Model.DBTable.TypeGood();
                typeGood.GoodType = new string(typeChar);
                db.TypeGoods.Add(typeGood);
                db.SaveChanges();
                IdSelect = (from t in db.TypeGoods
                            orderby t.Id descending
                            select t.Id).FirstOrDefault();
                Resources.Styles.NotifierClass notifierClass = new Resources.Styles.NotifierClass();
                notifierClass.notifier.ShowInformation("Додано новий тип товару: " + typeGood.GoodType);
                return IdSelect;
            }
            return IdSelect;
        }
        public List<Model.DBTable.SerialNumber> AddSNFunction()
        {
            List<Model.DBTable.SerialNumber> items = new List<Model.DBTable.SerialNumber>();
            if (SerialNumbers != null)
            {
                if (SerialNumbers.Count() > 0)
                {
                    items.AddRange(SerialNumbers);
                }
                if (items.Count > AllGood.Quantity)
                {
                    while (items.Count > AllGood.Quantity)
                    {
                        items.Remove(items.Last());
                    }
                    return items;
                }
            }
            while (items.Count < AllGood.Quantity)
            {
                Model.DBTable.SerialNumber serialNumber = new Model.DBTable.SerialNumber();
                items.Add(serialNumber);
            }
            return items;
        }
        /*public void DBBackupFunction()
        {
            string BackupDB = "BackupDB";
            if (!Directory.Exists(BackupDB))
            {
                Directory.CreateDirectory(BackupDB);
            }
            string name = "IdealStoreDB" + DateTime.Now.ToString("hh-mm_dd-MM-yyyy") + ".db";
            using (var source = new SQLiteConnection("Data Source=Resources/IdealStoreDB.db; Version=3;"))
            using (var destination = new SQLiteConnection("Data Source=BackupDB/"+name+"; Version=3;"))
            {
                source.Open();
                destination.Open();
                source.BackupDatabase(destination, "main", "main", -1, null, 0);
            }
        }*/
        public void BasketAddGoodFunction(Model.DBTable.Good good)
        {
            Model.DBTable.BasketGood basketGood = new Model.DBTable.BasketGood();
            basketGood = (from b in db.BasketGoods
                          where b.IdGood == good.Id
                          select b).FirstOrDefault();
            if (basketGood == null)
            {
                basketGood = new Model.DBTable.BasketGood();
                basketGood.IdGood = good.Id;
                basketGood.Quantity = 1;
                basketGood.SalePrice = good.SalePrice;
                db.BasketGoods.Add(basketGood);
                Resources.Styles.NotifierClass notifierClass = new Resources.Styles.NotifierClass();
                notifierClass.notifier.ShowInformation(good.Name + " додано в корзину.");
            }
            else
            {
                if (basketGood.Quantity < good.Quantity)
                {
                    basketGood.Quantity += 1;
                    db.Entry(basketGood).State = EntityState.Modified;
                    Resources.Styles.NotifierClass notifierClass = new Resources.Styles.NotifierClass();
                    notifierClass.notifier.ShowInformation(good.Name + " додано в корзину. Кількість: " + basketGood.Quantity +" шт.");
                }
                else
                {
                    Resources.Styles.NotifierClass notifierClass = new Resources.Styles.NotifierClass();
                    notifierClass.notifier.ShowError("Додано максимальну кількість товару.");
                }
            }
            db.SaveChanges();
        }
        
        public View.GoodView.AddGoodView addGoodViewFunction()
        {
            View.GoodView.AddGoodView addGoodView = new View.GoodView.AddGoodView();
            addGoodView.DataContext = AllGood;

            addGoodView.SNItemsControl.DataContext = this;

            TypeGoods = (from t in db.TypeGoods
                         orderby t.GoodType
                         select t).ToList();
            addGoodView.GoodTypeComboBox.ItemsSource = TypeGoods.Select(t => t.GoodType);

            addGoodView.PlusQuanButton.DataContext = this;
            addGoodView.PlusQuanButton.CommandParameter = addGoodView.SNItemsControl;

            addGoodView.MinusGuarButton.DataContext = this;
            addGoodView.MinusGuarButton.CommandParameter = addGoodView;

            addGoodView.MinusQuanButton.DataContext = this;
            addGoodView.MinusQuanButton.CommandParameter = addGoodView.SNItemsControl;

            addGoodView.PlusGuarButton.DataContext = this;
            addGoodView.PlusGuarButton.CommandParameter = addGoodView;

            addGoodView.SNCheckBox.DataContext = this;
            addGoodView.SNCheckBox.CommandParameter = addGoodView.SNItemsControl;

            addGoodView.AcceptAddRepButton.DataContext = this;
            addGoodView.AcceptAddRepButton.CommandParameter = addGoodView;

            addGoodView.NextGoodButton.DataContext = this;
            addGoodView.NextGoodButton.CommandParameter = addGoodView;

            return addGoodView;
        }
        
        public View.GoodView.CheckKodeGoodView checkKodeGoodViewFunction()
        {
            KodeStr = string.Empty;
            View.GoodView.CheckKodeGoodView checkKodeGoodView = new View.GoodView.CheckKodeGoodView();

            checkKodeGoodView.NextBut.CommandParameter = checkKodeGoodView;
            checkKodeGoodView.AcceptKeyCommand.CommandParameter = checkKodeGoodView;
            checkKodeGoodView.DataContext = this;
            checkKodeGoodView.KodeText.Focus();
            return checkKodeGoodView;
        }
        //-Command--------------------------------------------------------------
        public RelayCommand ClearGoodTypeFilter
        {
            get
            {
                return clearGoodTypeFilter ??
                    (clearGoodTypeFilter = new RelayCommand((o) =>
                    {
                        GoodTypeFilterText = null;
                    }));
            }
        }
        public RelayCommand FilterGoodAcceptCommand
        {
            get
            {
                return filterGoodAcceptCommand ??
                    (filterGoodAcceptCommand = new RelayCommand((o) =>
                    {
                        View.GoodView.FilterGoodView filterGoodView = o as View.GoodView.FilterGoodView;
                        filterGoodView.DialogResult = true;
                    }
                    ));
            }
        }
        public RelayCommand GoodFilterCommand
        {
            get
            {
                return goodFilterCommand ??
                    (goodFilterCommand = new RelayCommand((o) =>
                    {
                        View.GoodView.FilterGoodView filterGoodView = new View.GoodView.FilterGoodView();
                        filterGoodView.DataContext = this;
                        filterGoodView.AcceptFilterButton.CommandParameter = filterGoodView;
                        filterGoodView.TypeGoodFilterComboBox.ItemsSource = db.TypeGoods.OrderBy(arg => arg.GoodType).Select(arg => arg.GoodType).ToList();
                        if (filterGoodView.ShowDialog() == true)
                        {
                            AllGoodsFunction();
                        }
                    }));
            }
        }
        public RelayCommand AcceptReturnSaleCommand
        {
            get
            {
                return acceptReturnSaleCommand ??
                    (acceptReturnSaleCommand = new RelayCommand((comParam) =>
                    {
                        View.GoodView.ReturnGoodView returnGoodView = comParam as View.GoodView.ReturnGoodView;
                        bool returnGood = true;
                        if (DateReturnText == null || DateReturnText == string.Empty)
                        {
                            View.MessageBox.Show("Виберіть дату із списку.", "", MessageBoxButton.OK, "Окей", null, null);
                            returnGoodView.DateComboBox.BorderBrush = Brushes.Red;
                            returnGood = false;
                        }
                        else
                        {
                            if (returnGoodView.SNListContainer.Visibility == Visibility.Collapsed)
                            {

                            }
                            else
                            {
                                if (SNReturnText == null || SNReturnText == string.Empty)
                                {
                                    returnGood = false;
                                }
                                else
                                {
                                    Transactions = db.Transactions.Where(arg => arg.Date == DateReturnText).ToList();
                                    Model.DBTable.SerialNumber serialNumber = new Model.DBTable.SerialNumber();
                                    foreach (Model.DBTable.Transaction transaction in Transactions)
                                    {
                                        serialNumber = db.SerialNumbers.Where(arg => arg.IdTransaction == transaction.Id).Where(arg => arg.SN == SNReturnText).FirstOrDefault();
                                        if (serialNumber != null)
                                            break;
                                    }
                                    if (serialNumber == null)
                                    {
                                        View.MessageBox.Show("Введіть серійний із списку.", "", MessageBoxButton.OK, "Окей", null, null);
                                        returnGood = false;
                                        returnGoodView.SNComboBox.BorderBrush = Brushes.Red;

                                    }
                                }
                            }
                        }
                        if (returnGood == true)
                            returnGoodView.DialogResult = true;
                    }));
            }
        }
        public RelayCommand GoodReturnCommand
        {
            get
            {
                return goodReturnCommand ??
                    (goodReturnCommand = new RelayCommand((o) =>
                    {
                        KodeStr = string.Empty;
                        returnedGood = null;
                        View.GoodView.CheckKodeGoodView checkKodeGoodView = checkKodeGoodViewFunction();
                        if (checkKodeGoodView.ShowDialog() == true)
                        {
                            returnedGood = db.Goods.Where(arg => arg.Barcode == KodeStr).FirstOrDefault();
                            if (returnedGood != null)
                            {
                                string startDate = DateTime.Now.AddDays(-14).ToShortDateString();
                                Transactions = db.Transactions.ToList();
                                Transactions = Transactions.OrderByDescending(arg => arg.Id).Where(arg => DateTime.Parse(arg.Date) >= DateTime.Parse(startDate) && arg.Sum > 0).ToList();

                                SNReturnText = null;
                                DateReturnText = null;

                                View.GoodView.ReturnGoodView returnGoodView = new View.GoodView.ReturnGoodView();
                                returnGoodView.DataContext = this;

                                returnGoodView.ReturnSaleButton.CommandParameter = returnGoodView;

                                var ab = new BrushConverter();

                                ReturnComboBoxBorder = (Brush)ab.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyLightGrayBrush")));

                                returnGoodView.NameTextBlock.Text = returnedGood.Name;

                                Model.DBTable.Sale salesGood = new Model.DBTable.Sale();
                                foreach (Model.DBTable.Transaction transaction in Transactions)
                                {
                                    salesGood = db.Sales.Where(arg => arg.IdTransaction == transaction.Id && arg.IdGood == returnedGood.Id && arg.SalePrice > 0).FirstOrDefault();
                                    if (salesGood != null )
                                    {
                                        if (salesGood.Returned < salesGood.Quantity)
                                        {
                                            bool addNewItem = true;
                                            if (returnGoodView.DateComboBox.Items.Count > 0)
                                                foreach (var itemComboBox in returnGoodView.DateComboBox.Items)
                                                {
                                                    if (itemComboBox.ToString() == transaction.Date.ToString())
                                                    {
                                                        addNewItem = false;
                                                        break;
                                                    }
                                                }
                                            if (addNewItem == true)
                                                returnGoodView.DateComboBox.Items.Add(transaction.Date);
                                        }
                                    }
                                }
                                if (returnGoodView.DateComboBox.Items.Count == 0)
                                {
                                    View.MessageBox.Show("За останні 14 днів, даний товар не продавався.", "", MessageBoxButton.OK, "Окей", null, null);
                                    return;
                                }
                                if (returnedGood.SN == 1)
                                {
                                    returnGoodView.SNListContainer.Visibility = Visibility.Visible;
                                }

                                if (returnGoodView.ShowDialog() == true)
                                {
                                    int retSalePrice = db.Sales.Where(arg => arg.IdTransaction == db.Transactions.Where(arg1 => arg1.Date == DateReturnText).Select(arg1 => arg1.Id).FirstOrDefault() && arg.IdGood == returnedGood.Id).Select(arg => arg.SalePrice).FirstOrDefault();
                                    if (retSalePrice > 0)
                                    {
                                        MessageBoxResult messageBoxResult = View.MessageBox.Show("Поверненути " + returnedGood.Name + " за " + DateReturnText + ".\nСумма повернення "
                                            + retSalePrice + "грн.", "", MessageBoxButton.YesNo, "Повернути", "Скасувати", "");
                                        if (messageBoxResult == MessageBoxResult.Yes)
                                        {
                                            returnedGood.Quantity++;
                                            db.Entry(returnedGood).State = EntityState.Modified;
                                            if (returnedGood.SN == 1)
                                            {
                                                Model.DBTable.SerialNumber serialNumber = db.SerialNumbers.Where(arg => arg.SN == SNReturnText).FirstOrDefault();
                                                if (serialNumber.SN != null)
                                                {
                                                    serialNumber.IdTransaction = 0;
                                                    db.Entry(serialNumber).State = EntityState.Modified;
                                                }
                                            }
                                            Model.DBTable.Transaction transaction = new Model.DBTable.Transaction();
                                            transaction.Cash = 0;
                                            transaction.Date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                            transaction.Sum = -retSalePrice;
                                            //transaction.Returned = 1;
                                            db.Transactions.Add(transaction);
                                            transaction = db.Transactions.Where(arg => arg.Date.ToString() == returnGoodView.DateComboBox.Text).FirstOrDefault();
                                            db.Entry(transaction).State = EntityState.Modified;
                                            db.SaveChanges();

                                            Model.DBTable.Sale sale = new Model.DBTable.Sale();
                                            sale.SalePrice = -retSalePrice;
                                            sale.Quantity = 1;
                                            sale.IdTransaction = db.Transactions.OrderByDescending(arg => arg.Id).Select(arg => arg.Id).FirstOrDefault();
                                            sale.IdGood = returnedGood.Id;
                                            db.Sales.Add(sale);

                                            sale = db.Sales.Where(arg => arg.IdTransaction == db.Transactions.Where(arg1 => arg1.Date == DateReturnText).Select(arg1 => arg1.Id).FirstOrDefault() && arg.IdGood == returnedGood.Id).FirstOrDefault();
                                            sale.Returned++;
                                            db.Entry(sale).State = EntityState.Modified;
                                            db.SaveChanges();
                                            AllGoodsFunction();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                View.MessageBox.Show("Даного товару не існує в базі.","",MessageBoxButton.OK,"Окей", null, null);
                            }
                        }
                    }
                    ));
            }
        }
        public RelayCommand DropDownClosed
        {
            get
            {
                return dropDownClosed ??
                    (dropDownClosed = new RelayCommand((commandParameter) =>
                    {
                        View.GoodView.ReturnGoodView returnGoodView = commandParameter as View.GoodView.ReturnGoodView;
                        if (returnGoodView.DateComboBox.SelectedItem != null)
                        {
                            var ab = new BrushConverter();
                            returnGoodView.DateComboBox.BorderBrush = (Brush)ab.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyLightGrayBrush")));
                            Transactions = db.Transactions.Where(arg => arg.Date == returnGoodView.DateComboBox.Text).ToList();
                            returnGoodView.SNComboBox.Items.Clear();
                            returnGoodView.SNComboBox.IsEnabled = true;
                            returnGoodView.SNListContainer.ClearValue(StackPanel.ToolTipProperty);
                            foreach (Model.DBTable.Transaction transaction in Transactions)
                            {
                                SerialNumbers = db.SerialNumbers.Where(arg => arg.IdTransaction == transaction.Id).Where(arg => arg.IdGood == returnedGood.Id).ToList();
                                foreach (Model.DBTable.SerialNumber serialNumber in SerialNumbers)
                                    returnGoodView.SNComboBox.Items.Add(serialNumber.SN);
                            }
                        }
                    }));
            }
        }
        public RelayCommand InBasketGoodCommand
        {
            get
            {
                return inBasketGoodCommand ??
                    (inBasketGoodCommand = new RelayCommand((selectedItem) =>
                    {
                        if (selectedItem == null)
                            return;
                        Model.AllGood allGood = selectedItem as Model.AllGood;
                        Model.DBTable.Good good = (from g in db.Goods
                                                   where g.Id == allGood.Id
                                                   select g).FirstOrDefault();
                        BasketAddGoodFunction(good);
                    }));
            }
        }
        public RelayCommand GoodSearchCommand
        {
            get
            {
                return goodSearchCommand ??
                    (goodSearchCommand = new RelayCommand((o) =>
                    {
                        AllGoodsFunction();
                    }));
            }
        }
        public RelayCommand GoodAddCommand
        {
            get
            {
                return goodAddCommand ??
                  (goodAddCommand = new RelayCommand((o) =>
                  {
                      nextGood = true;
                      while (nextGood == true)
                      {
                          KodeStr = null;
                          QuanSimGood = 0;
                          SerialNumbers = null;
                          SNCheckBox_IsChecked = false;

                          View.GoodView.CheckKodeGoodView checkKodeGoodView = checkKodeGoodViewFunction();
                          if (checkKodeGoodView.ShowDialog() == true)
                          {
                              SerialNumbers = SerialNumbers;
                              bool newGood = true;
                              AllGood = (from g in db.Goods
                                         join t in db.TypeGoods
                                         on g.GoodType equals t.Id
                                         where g.Barcode == KodeStr
                                         select new Model.AllGood
                                         {
                                             Id = g.Id,
                                             Kode = g.Kode,
                                             Name = g.Name,
                                             Price = g.Price,
                                             SalePrice = g.SalePrice,
                                             Quantity = g.Quantity,
                                             Guarante = g.Guarante,
                                             GoodType = t.GoodType,
                                             Barcode = g.Barcode,
                                             Description = g.Description,
                                             Used = g.Used,
                                             Deleted = g.Deleted,
                                             SN = g.SN
                                         }).FirstOrDefault();

                              //AllGood = AllGoods.Where(arg => arg.Barcode.Equals(KodeStr)).FirstOrDefault();
                              if (AllGood != null)
                              {
                                  MessageBoxResult messageBoxResult = View.MessageBox.Show("Ви ввели код, який існує в базі." +
                                      "Ви зможете відредагувати даний товар,\n для цього натисніть кнопку продовжити.", "", MessageBoxButton.OKCancel, "Продовжити", "Скасувати", null);
                                  if (messageBoxResult == MessageBoxResult.Cancel || messageBoxResult == MessageBoxResult.None)
                                  {
                                      AllGoodsFunction();
                                      return;
                                  }     
                                  newGood = false;
                                  if (AllGood.Deleted == 1)
                                  {
                                      AllGood.Deleted = 0;
                                      AllGood.Quantity = 0;
                                  }
                                  QuanSimGood = AllGood.Quantity;
                                  AllGood.Quantity = 1;
                                  if (AllGood.SN == 1)
                                  {
                                      List<Model.DBTable.SerialNumber> newSerials = new List<Model.DBTable.SerialNumber>();
                                      newSerials.Add(new Model.DBTable.SerialNumber());
                                      SerialNumbers = newSerials;
                                  }
                              }
                              else
                              {
                                  AllGood = new Model.AllGood();
                                  AllGood.Barcode = KodeStr;
                                  AllGood.Quantity = 1;
                              }
                              View.GoodView.AddGoodView addGoodView = addGoodViewFunction();

                              if (newGood == false)
                              {
                                  addGoodView.NameText.IsEnabled = false;
                                  addGoodView.KodeTextBox.IsEnabled = false;
                                  addGoodView.GoodTypeComboBox.IsEnabled = false;
                                  addGoodView.BarcodeText.IsEnabled = false;
                                  addGoodView.DescriptionText.IsEnabled = false;

                                  addGoodView.UsedCheckBox.IsEnabled = false;
                                  addGoodView.SNCheckBox.IsEnabled = false;
                                  if (AllGood.SN == 1)
                                  {
                                      SNCheckBox_IsChecked = true;
                                      addGoodView.SNItemsControl.Items.Add(SerialNumbers);
                                  }
                                  if (AllGood.Used == 1)
                                  {
                                      addGoodView.UsedCheckBox.IsChecked = true;
                                  }
                              }
                              KodeStr = null;
                              if (addGoodView.ShowDialog() == true)
                              {
                                  if (newGood == true)
                                  {
                                      Model.DBTable.Good good = new Model.DBTable.Good();
                                      good.Kode = AllGood.Kode;
                                      good.Name = AllGood.Name != null ? AllGood.Name : string.Empty;
                                      good.Barcode = AllGood.Barcode != null ? AllGood.Barcode : string.Empty;
                                      good.Description = AllGood.Description != null ? AllGood.Description : string.Empty;
                                      good.Price = AllGood.Price;
                                      good.SalePrice = AllGood.SalePrice;
                                      good.Quantity = AllGood.Quantity;
                                      good.Guarante = AllGood.Guarante;
                                      if (SNCheckBox_IsChecked == true)
                                          good.SN = 1;
                                      if (addGoodView.UsedCheckBox.IsChecked == true)
                                          good.Used = 1;

                                      good.GoodType = TypeIdCheckFunction(AllGood.GoodType);

                                      if (newGood == true)
                                      {
                                          db.Goods.Add(good);
                                          db.SaveChanges();
                                          if (good.SN == 1)
                                          {
                                              int IdGood = (from g in db.Goods
                                                            orderby g.Id descending
                                                            select g.Id).FirstOrDefault();
                                              foreach (Model.DBTable.SerialNumber serialNumber in SerialNumbers)
                                              {
                                                  serialNumber.IdGood = IdGood;
                                                  db.SerialNumbers.Add(serialNumber);
                                              }
                                          }
                                      }
                                  }
                                  else if (newGood == false)
                                  {
                                      Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == AllGood.Id).FirstOrDefault();
                                      good.Price = AllGood.Price;
                                      good.SalePrice = AllGood.SalePrice;
                                      good.Quantity += AllGood.Quantity;
                                      db.Entry(good).State = EntityState.Modified;
                                      if (good.SN == 1)
                                      {
                                          foreach (Model.DBTable.SerialNumber serialNumber in SerialNumbers)
                                          {
                                              serialNumber.IdGood = good.Id;
                                              db.SerialNumbers.Add(serialNumber);
                                          }
                                      }
                                  }
                                  db.SaveChanges();
                              }
                              else                                  
                                  nextGood = false;
                          }
                          else              
                              nextGood = false;
                      }
                      AllGoodsFunction();
                      //DBBackupFunction();
                  }));
            }
        }
        public RelayCommand GoodEditCommand
        {
            get
            {
                return goodEditCommand ??
                  (goodEditCommand = new RelayCommand((selectedItem) =>
                  {
                      if (selectedItem == null)
                          return;
                      AllGood = selectedItem as Model.AllGood;
                      //Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == allGood.Id).FirstOrDefault();
                      if (AllGood != null)
                      {
                          View.GoodView.AddGoodView addGoodView = addGoodViewFunction();
                          addGoodView.QuanText.IsEnabled = false;
                          addGoodView.NextGoodButton.Visibility = Visibility.Collapsed;
                          addGoodView.PlusQuanButton.IsEnabled = false;
                          addGoodView.MinusQuanButton.IsEnabled = false;
                          if (AllGood.SN == 1)
                          {
                              SNCheckBox_IsChecked = true;
                              SerialNumbers = db.SerialNumbers.Where(arg => arg.IdGood == AllGood.Id).Where(arg => arg.IdTransaction == 0).ToList();
                              addGoodView.SNItemsControl.ItemsSource = SerialNumbers;
                          }
                          addGoodView.SNCheckBox.IsEnabled = false;
                          
                          
                          if(addGoodView.ShowDialog() == true)
                          {
                              Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == AllGood.Id).FirstOrDefault();
                              if (good != null)
                              {
                                  good.Kode = AllGood.Kode != null ? AllGood.Kode : string.Empty;
                                  good.Name = AllGood.Name != null ? AllGood.Name : string.Empty;
                                  good.Barcode = AllGood.Barcode != null ? AllGood.Barcode : string.Empty;
                                  good.Description = AllGood.Description != null ? AllGood.Description : string.Empty;
                                  good.Price = AllGood.Price;
                                  good.SalePrice = AllGood.SalePrice;
                                  good.Guarante = AllGood.Guarante;
                                  
                                  if (addGoodView.UsedCheckBox.IsChecked == true)
                                      good.Used = 1;
                                  else
                                      good.Used = 0;

                                  SerialNumbers = db.SerialNumbers.Where(arg => arg.IdGood == AllGood.Id).Where(arg => arg.IdComputer == 0).ToList();
                                  if (good.SN == 1)
                                  {
                                      foreach (Model.DBTable.SerialNumber serialNumber in SerialNumbers)
                                      {
                                          db.Entry(serialNumber).State = EntityState.Modified;
                                      }
                                  }

                                  SerialNumbers = db.SerialNumbers.Where(arg => arg.IdGood == AllGood.Id).ToList();

                                  good.GoodType = TypeIdCheckFunction(AllGood.GoodType);

                                  db.Entry(good).State = EntityState.Modified;
                                  db.SaveChanges();
                                  // Used SN GoodType
                                  // Змінювати кількість приредагуванні не можна

                              }
                          }
                      }
                      AllGoodsFunction();
                  }));
            }
        }
        public RelayCommand GoodDeleteCommand
        {
             get
             {
                 return goodDeleteCommand ??
                   (goodDeleteCommand = new RelayCommand((selectedItem) =>
                   {
                       if (selectedItem == null)
                           return;
                       Model.AllGood allGood = selectedItem as Model.AllGood;
                       Model.DBTable.Good good = (from g in db.Goods
                                                  where g.Id == allGood.Id
                                                  select g).FirstOrDefault();
                       MessageBoxResult result1 = View.MessageBox.Show("Видалити товар з бази даних?", "", MessageBoxButton.YesNo, "Так", "Ні", null);
                       switch (result1)
                       {
                           /*
                            -------------------------------------------------------
                            Видалення серійних номерів
                            -------------------------------
                            */
                           case MessageBoxResult.Yes:
                               good.Deleted = 1;
                               good.Quantity = 0;
                               db.Entry(good).State = EntityState.Modified;
                               Model.DBTable.BasketGood basketGood = db.BasketGoods.Where(arg => arg.IdGood == good.Id).FirstOrDefault();
                               if (basketGood != null)
                               {
                                   db.Entry(basketGood).State = EntityState.Deleted;
                               }
                               SerialNumbers = db.SerialNumbers.Where(arg => arg.IdGood == good.Id).Where(arg => arg.IdTransaction == 0 && arg.IdComputer == 0).ToList();
                               if (SerialNumbers.Count() > 0)
                               {
                                   foreach (Model.DBTable.SerialNumber serialNumber in SerialNumbers)
                                   {
                                       db.Entry(serialNumber).State = EntityState.Deleted;
                                   }
                               }
                               db.SaveChanges();
                               AllGoodsFunction();
                               break;
                           case MessageBoxResult.No:
                               break;
                       }
                   }));
             }
        }
        public RelayCommand GoodPlusQuanCommand
        {
            get
            {
                return goodPlusQuanCommand ??
                    (goodPlusQuanCommand = new RelayCommand((o) =>
                    {
                        AllGood.Quantity++;
                        if (SNCheckBox_IsChecked == true)
                        {
                            var itemsControl = o as ItemsControl;
                            SerialNumbers = AddSNFunction();
                            itemsControl.Items.Add(SerialNumbers.Last());
                        }
                    }));
            }
        }
        public RelayCommand GoodMinusQuanCommand
        {
            get
            {
                return goodMinusQuanCommand??
                    (goodMinusQuanCommand = new RelayCommand((o) =>
                    {
                        if (AllGood.Quantity > 0)
                            AllGood.Quantity--;
                        if (SNCheckBox_IsChecked == true)
                        {
                            var itemsControl = o as ItemsControl;
                            if (itemsControl.Items.Count > 1)
                            {
                                itemsControl.Items.Remove(itemsControl.Items[(itemsControl.Items.Count) - 1]);
                                SerialNumbers = AddSNFunction();
                            }
                        }
                    }));
            }
        }
        public RelayCommand GoodPlusGuaranteCommand
        {
            get
            {
                return goodPlusGuaranteCommand ??
                    (goodPlusGuaranteCommand = new RelayCommand((o) =>
                    {
                        AllGood.Guarante++;
                    }));
            }
        }
        public RelayCommand GoodMinusGuaranteCommand
        {
            get
            {
                return goodMinusGuaranteCommand ??
                    (goodMinusGuaranteCommand = new RelayCommand((o) =>
                    {
                        AllGood.Guarante--;
                    }));
            }
        }
        public RelayCommand ClearKodeTextCommand
        {
            get
            {
                return clearKodeTextCommand ??
                    (clearKodeTextCommand = new RelayCommand((o) =>
                    {
                        KodeStr = string.Empty;
                    }));
            }
        }
        public RelayCommand AcceptCheckKodeButtonCommand
        {
            get
            {
                return acceptButtonCommand ??
                    (acceptButtonCommand = new RelayCommand((o) =>
                    {
                        var window = o as Window;
                        if (KodeStr.Length > 0)
                        {
                            window.DialogResult = true;
                        }
                    }));
            }
        }
        public RelayCommand SNCheckBoxChangeCommand
        {
            get
            {
                return sNCheckBoxChangeCommand ??
                    (sNCheckBoxChangeCommand = new RelayCommand((o) =>
                    {
                        if (SNCheckBox_IsChecked == true)
                        {
                            var itemsControl = o as ItemsControl;
                            itemsControl.Items.Clear();
                            SerialNumbers = AddSNFunction();
                            foreach (Model.DBTable.SerialNumber serialNumber in SerialNumbers)
                            {
                                itemsControl.Items.Add(serialNumber);
                            }
                        }
                    }
                    ));
            }
        }
        public RelayCommand AcceptAddGoodButtonCommand
        {
            get
            {
                return acceptAddGoodButtonCommand ??
                    (acceptAddGoodButtonCommand = new RelayCommand((o) =>
                    {
                        var window = o as View.GoodView.AddGoodView;
                        bool addError = false;
                        var ab = new BrushConverter();

                        if (AllGood.Name == null || AllGood.Name.Length == 0)
                        {
                            window.NameText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                            window.NameText.BorderBrush = Brushes.Red;
                            addError = true;
                        }
                        if (AllGood.GoodType == null || AllGood.GoodType.Length == 0)
                        {
                            window.GoodTypeComboBox.Background = (Brush)ab.ConvertFrom("#FFDADA");
                            window.GoodTypeComboBox.BorderBrush = Brushes.Red;
                            addError = true;
                        }
                        if (AllGood.Barcode == null || AllGood.Barcode.Length == 0)
                        {
                            window.BarcodeText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                            window.BarcodeText.BorderBrush = Brushes.Red;
                            addError = true;
                        }
                        if (AllGood.SalePrice == 0 || AllGood.Price > AllGood.SalePrice)
                        {
                            window.SalePriceText.Background = (Brush)ab.ConvertFrom("#FFDADA");
                            window.SalePriceText.BorderBrush = Brushes.Red;
                            addError = true;
                        }
                        if (SNCheckBox_IsChecked == true)
                        {
                            foreach (Model.DBTable.SerialNumber serialNumber in SerialNumbers)
                            {
                                if (serialNumber.SN == null || serialNumber.SN == string.Empty)
                                {
                                    window.SNItemsControl.BorderBrush = Brushes.Red;
                                    addError = true;
                                }
                                if (window.NextGoodButton.Visibility == Visibility.Visible)
                                {
                                    Model.DBTable.SerialNumber similarSN = db.SerialNumbers.Where(arg => arg.SN == serialNumber.SN).FirstOrDefault();
                                    if (similarSN != null)
                                    {
                                        window.SNItemsControl.BorderBrush = Brushes.Red;
                                        addError = true;
                                        View.MessageBox.Show("Введено серійний номер, який існує в базі.", "", MessageBoxButton.OK, "Окей", null, null);
                                        return;
                                    }
                                }
                            }
                            int CheckQuan = SerialNumbers.Count();
                            if (SerialNumbers.Count() != SerialNumbers.GroupBy(arg => arg.SN).Count())
                            {
                                window.SNItemsControl.BorderBrush = Brushes.Red;
                                addError = true;
                            }
                        }

                        if (addError == false)
                        {
                            if (window.NextGoodButton.IsMouseOver == true)
                            {
                                nextGood = true;
                            }
                            else
                                nextGood = false;
                            window.DialogResult = true;
                        }    
                    }));
            }
        }
        //-------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

    }
}