﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Data.SQLite;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using System.Windows.Shapes;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Xaml.Behaviors;

namespace IdealStore.ViewModel
{
    public class MainViewModel : DependencyObject, INotifyPropertyChanged
    {
        ApplicationContext db;
        /*private Page BasketPage;
        private Page GoodPage;
        private Page ConfiguratorPage;
        private Page IncomePage;
        private Page PCPage;
        private Page RepairPage;*/
        private Page page = new Page();
        private BrushConverter brushConv = new BrushConverter();

        /*int inBasketCount;

        public int InBasketCount
        {
            get { return inBasketCount; }
            set
            {
                inBasketCount = value;
                OnPropertyChanged("InBasketCount");
            }
        }*/

        public View.MainView mainView;

        private Page _currentPage;
        public Page CurrentPage
        {
            get { return _currentPage; }
            set
            {
                if (_currentPage == value)
                    return;

                _currentPage = value;
                OnPropertyChanged("CurrentPage");
            }
        }
        private double _frameOpacity;
        public double FrameOpacity
        {
            get { return _frameOpacity; }
            set
            {
                if (_frameOpacity == value)
                    return;

                _frameOpacity = value;
                OnPropertyChanged("FrameOpacity");
            }
        }


        //-------------------------------------
        public MainViewModel(View.MainView mainView)
        {
            db = new ApplicationContext();
            mainView.GoodPageButton.Background = (Brush)brushConv.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyLightBlueBrush")));
            CurrentPage = new View.Pages.GoodPage();
            FrameOpacity = 1;
            Function.DBBackupFunctionClass dBBackupFunctionClass = new Function.DBBackupFunctionClass();
            dBBackupFunctionClass.DBBackupFunction();
            //InBasketCount = db.BasketGoods.Count();
        }
        //---Function--------------------------------------------
        private async void SlowOpacityFunction(Page page)
        {
            mainView.MainGrid.IsEnabled = false;
            await Task.Factory.StartNew(() =>
            {
                for (double i = 1.0; i > 0.0; i -= 0.1)
                {
                    FrameOpacity = i;
                    Thread.Sleep(30);
                }
                CurrentPage = page;
                for (double i = 0.0; i < 1.1; i += 0.1)
                {
                    FrameOpacity = i;
                    Thread.Sleep(30);
                }
            });
            mainView.MainGrid.IsEnabled = true;
        }
        //--Command----------------------------------------------
        public ICommand AddNewCommand
        {
            get
            {
                return new RelayCommand((o) =>
                {
                    db = new ApplicationContext();
                    if (CurrentPage.Title == "GoodPage")
                    {
                        ViewModel.GoodViewModel goodViewModel = CurrentPage.DataContext as ViewModel.GoodViewModel;
                        goodViewModel.KodeStr = String.Empty;
                        View.GoodView.CheckKodeGoodView checkKodeGoodView = new View.GoodView.CheckKodeGoodView(); 
                        checkKodeGoodView.NextBut.CommandParameter = checkKodeGoodView;
                        checkKodeGoodView.AcceptKeyCommand.CommandParameter = checkKodeGoodView;
                        checkKodeGoodView.DataContext = goodViewModel;
                        checkKodeGoodView.KodeText.Focus();
                        if (checkKodeGoodView.ShowDialog() == true)
                        {
                            Model.DBTable.Good good = new Model.DBTable.Good();
                            good = db.Goods.Where(arg => arg.Barcode == goodViewModel.KodeStr && arg.Deleted == 0 && arg.Quantity > 0).FirstOrDefault();
                            if (good != null)
                                goodViewModel.BasketAddGoodFunction(good);
                            else
                                View.MessageBox.Show("Просканованого штрих-коду не існує в базі данних.\nПродаж не можливий.","",MessageBoxButton.OK, "Окей", null, null);
                        }
                    }
                    /*if (CurrentPage.Title == "LaptopPage")
                    {
                        ViewModel.LaptopViewModel laptopViewModel = CurrentPage.DataContext as ViewModel.LaptopViewModel;
                        laptopViewModel.KodeStr = String.Empty;
                        View.GoodView.CheckKodeGoodView checkKodeGoodView = new View.GoodView.CheckKodeGoodView();
                        checkKodeGoodView.NextBut.CommandParameter = checkKodeGoodView;
                        checkKodeGoodView.AcceptKeyCommand.CommandParameter = checkKodeGoodView;
                        checkKodeGoodView.DataContext = laptopViewModel;
                        checkKodeGoodView.KodeText.Focus();
                        if (checkKodeGoodView.ShowDialog() == true)
                        {
                            Model.DBTable.Good good = new Model.DBTable.Good();
                            good = db.Goods.Where(arg => arg.Barcode == laptopViewModel.KodeStr && arg.Deleted == 0 && arg.Quantity > 0).FirstOrDefault();
                            if (good != null)
                            {
                                View.Pages.LaptopPage laptopPage = CurrentPage as View.Pages.LaptopPage;
                                laptopPage.computersList.SelectedItem = new Model.AllLaptop();
                            }
                            else
                                View.MessageBox.Show("Просканованого штрих-коду не існує в базі данних.\nПродаж не можливий.", "", MessageBoxButton.OK, "Окей", null, null);
                        }
                    }*/
                });
            }
        }
        public ICommand ShowPageCommand
        {
            get
            {
                return new RelayCommand((o) =>
                {
                    mainView = o as View.MainView;
                    string butName = String.Empty;
                    foreach (object obj in mainView.MainGrid.Children)
                    {
                        Button button = obj as Button;
                        if (button != null)
                        {
                            button.Background = Brushes.Transparent;
                            if (button.IsFocused == true)
                            {
                                button.Background = (Brush)brushConv.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyLightBlueBrush")));
                                butName = button.Name;
                            }
                        }
                    }

                    switch (butName)
                    {
                        case "RepairPageButton":
                            page = new View.Pages.RepairPage();
                            if (page.Title != CurrentPage.Title)
                                SlowOpacityFunction(page);
                            break;
                        case "GoodPageButton":
                            page = new View.Pages.GoodPage();
                            if (page.Title != CurrentPage.Title)
                                SlowOpacityFunction(page);
                            break;
                        case "BasketPageButton":
                            page = new View.Pages.BasketPage();
                            if (page.Title != CurrentPage.Title)
                                SlowOpacityFunction(page);
                            break;
                        case "ConfiguratorPageButton":
                            page = new View.Pages.ConfiguratorPage();
                            if (page.Title != CurrentPage.Title)
                                SlowOpacityFunction(page);
                            break;
                        case "PCPageButton":
                            page = new View.Pages.PCPage();
                            if (page.Title != CurrentPage.Title)
                                SlowOpacityFunction(page);
                            break;
                        case "LaptopPageButton":
                            page = new View.Pages.LaptopPage();
                            if (page.Title != CurrentPage.Title)
                                SlowOpacityFunction(page);
                            break;
                        case "IncomePageButton":
                            page = new View.Pages.IncomePage();
                            if (page.Title != CurrentPage.Title)
                                SlowOpacityFunction(page);
                            break;
                        
                    }
                }
                );
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
