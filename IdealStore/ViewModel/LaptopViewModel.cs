﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Data.SQLite;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using System.Windows.Shapes;
using DevExpress.Mvvm.Native;
using IdealStore.Model;
using System.Windows.Documents;
using Microsoft.Xaml.Behaviors;
using ToastNotifications.Utilities;
using IdealStore.Model.DBTable;
using Xceed.Wpf.Toolkit.Converters;

namespace IdealStore.ViewModel
{
    public class LaptopViewModel : INotifyPropertyChanged
    {
        ApplicationContext db;
        RelayCommand filterComboBoxDropDownClosedCommand;
        RelayCommand deleteLaptopCommand;
        RelayCommand saleLaptopCommand;
        RelayCommand editLaptopCommand;
        RelayCommand acceptAddEditLaptopCommand;
        RelayCommand addLaptopCommand;
        RelayCommand laptopSearchCommand;


        string filterParam;
        string kodeStr;

        public string KodeStr
        {
            get { return kodeStr; }
            set
            {
                kodeStr = value;
                OnPropertyChanged("KodeStr");
            }
        }


        ObservableCollection<Model.AllLaptop> allLaptops;
        IEnumerable<Model.DBTable.Good> goods;

        public ObservableCollection<Model.AllLaptop> AllLaptops
        {
            get { return allLaptops; }
            set
            {
                allLaptops = value;
                OnPropertyChanged("AllLaptops");
            }
        }
        public IEnumerable<Model.DBTable.Good> Goods
        {
            get { return goods; }
            set
            {
                goods = value;
                OnPropertyChanged("Goods");
            }
        }

        void ShowLaptopFunction()
        {
            //Goods = db.Goods.Where(arg => arg.GoodType == 29).ToList();
            AllLaptops = (from g in db.Goods
                          join s in db.SerialNumbers
                          on g.Id equals s.IdGood
                          where g.GoodType == 29 && g.Deleted == 0 && g.Quantity > 0
                          select new Model.AllLaptop
                          {
                              Id = g.Id,
                              Name = g.Name,
                              Price = g.Price,
                              SalePrice = g.SalePrice,
                              Barcode = g.Barcode,
                              Description = g.Description,
                              Used = g.Used == 0 ? "Новий" : "Б/в",
                              SN = s.SN,
                              Guarante = g.Guarante
                          }).ToObservableCollection();
            if (filterParam == "Нові")
                AllLaptops = AllLaptops.Where(arg => arg.Used == "Новий").ToObservableCollection();
            if (filterParam == "Б/в")
                AllLaptops = AllLaptops.Where(arg => arg.Used == "Б/в").ToObservableCollection();
        }
        public LaptopViewModel()
        {
            db = new ApplicationContext();
            ShowLaptopFunction();
        }

        public RelayCommand LaptopSearchCommand
        {
            get
            {
                return laptopSearchCommand ??
                    (laptopSearchCommand = new RelayCommand((parametr) =>
                    {
                        string searchText = parametr.ToString();
                        ShowLaptopFunction();
                        if (searchText == null || searchText == string.Empty)
                            return;
                        AllLaptops = AllLaptops.Where(arg => arg.Name.ToLower().Contains(searchText.ToLower()) || arg.Description.ToLower().Contains(searchText.ToLower())).ToObservableCollection();
                    }));
            }
        }
        public RelayCommand AcceptAddEditLaptopCommand
        {
            get
            {
                return acceptAddEditLaptopCommand ??
                    (acceptAddEditLaptopCommand = new RelayCommand((parametr) =>
                    {
                        View.LaptopView.AddEditLaptopView addEditLaptopView = parametr as View.LaptopView.AddEditLaptopView;
                        if (addEditLaptopView == null)
                            return;
                        Model.AllLaptop allLaptop = addEditLaptopView.DataContext as Model.AllLaptop;
                        bool addError = false;
                        if(allLaptop.Name == null || allLaptop.Name == string.Empty)
                        {
                            addError = true;
                        }
                        if (allLaptop.SalePrice == 0)
                        {
                            addError = true;
                        }
                        if (allLaptop.Barcode == null || allLaptop.Barcode == string.Empty)
                        {
                            addError = true;
                        }
                        if (allLaptop.Description == null || allLaptop.Description == string.Empty)
                        {
                            addError = true;
                        }
                        if (addEditLaptopView.Title == "Додавання ноутбука")
                        {
                            if (allLaptop.SN == null || allLaptop.SN == string.Empty)
                                addError = true;
                            else
                            {
                                Model.DBTable.SerialNumber serialNumber = db.SerialNumbers.Where(arg => arg.SN == allLaptop.SN).FirstOrDefault();
                                if(serialNumber != null)
                                {
                                    View.MessageBox.Show("Введений серійний номер уже існує в базі.", "", MessageBoxButton.OK, "Окей", null, null);
                                    return;
                                }
                            }
                        }
                        if (addError == false)
                            addEditLaptopView.DialogResult = true;
                        else
                            View.MessageBox.Show("Перевірьте коректність введених даних.", "", MessageBoxButton.OK, "Окей", "", "");
                    }));
                    
            }
        }
        public RelayCommand AddLaptopCommand
        {
            get
            {
                return addLaptopCommand ??
                    (addLaptopCommand = new RelayCommand((parametr) =>
                    {
                        Model.AllLaptop allLaptop = new Model.AllLaptop();
                        View.LaptopView.AddEditLaptopView addEditLaptopView = new View.LaptopView.AddEditLaptopView();
                        addEditLaptopView.DataContext = allLaptop;
                        addEditLaptopView.Title = "Додавання ноутбука";//При змінні назви змінити її в AcceptEditAddLaptopCommand
                        addEditLaptopView.ButtonStackPanel.DataContext = this;
                        addEditLaptopView.SNStackPanel.Visibility = Visibility.Visible;
                        addEditLaptopView.PriceStackPanel.Visibility = Visibility.Visible;
                        if (addEditLaptopView.ShowDialog() == true)
                        {
                            Model.DBTable.Good good = new Model.DBTable.Good();
                            good.Name = allLaptop.Name;
                            good.SalePrice = allLaptop.SalePrice;
                            good.Barcode = allLaptop.Barcode;
                            good.Description = allLaptop.Description;
                            if (addEditLaptopView.UsedCheckBox.IsChecked == true)
                                good.Used = 1;
                            else
                                good.Used = 0;
                            good.Guarante = allLaptop.Guarante;
                            good.SN = 1;
                            good.Price = allLaptop.Price;
                            good.Quantity = 1;
                            good.GoodType = 29;
                            db.Goods.Add(good);
                            db.SaveChanges();
                            Model.DBTable.SerialNumber serialNumber = new Model.DBTable.SerialNumber();
                            serialNumber.SN = allLaptop.SN;
                            serialNumber.IdGood = db.Goods.OrderByDescending(arg => arg.Id).Select(arg => arg.Id).FirstOrDefault();
                            db.SerialNumbers.Add(serialNumber);
                            db.SaveChanges();

                        }
                        ShowLaptopFunction();
                    }));
            }
        }
        public RelayCommand EditLaptopCommand
        {
            get
            {
                return editLaptopCommand ??
                    (editLaptopCommand = new RelayCommand((parametr) =>
                    {
                        Model.AllLaptop allLaptop = parametr as Model.AllLaptop;
                        View.LaptopView.AddEditLaptopView addEditLaptopView = new View.LaptopView.AddEditLaptopView();
                        addEditLaptopView.DataContext = allLaptop;
                        addEditLaptopView.ButtonStackPanel.DataContext = this;
                        if (allLaptop.Used == "Б/в")
                            addEditLaptopView.UsedCheckBox.IsChecked = true;
                        if (addEditLaptopView.ShowDialog() == true)
                        {
                            Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == allLaptop.Id).FirstOrDefault();
                            good.Name = allLaptop.Name;
                            good.SalePrice = allLaptop.SalePrice;
                            good.Barcode = allLaptop.Barcode;
                            good.Description = allLaptop.Description;
                            //good.Used = allLaptop.Used;
                            if (addEditLaptopView.UsedCheckBox.IsChecked == true)
                                good.Used = 1;
                            else
                                good.Used = 0;
                            good.Guarante = allLaptop.Guarante;
                            db.Entry(good).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        ShowLaptopFunction();
                    }));
            }
        }
        public RelayCommand SaleLaptopCommand
        {
            get
            {
                return saleLaptopCommand ??
                    (saleLaptopCommand = new RelayCommand((parametr) =>
                    {
                        Model.AllLaptop allLaptop = parametr as Model.AllLaptop;
                        if (allLaptop == null)
                            return;
                        //Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == allLaptop.Id).FirstOrDefault();
                        Model.DBTable.BasketGood basketGood = new Model.DBTable.BasketGood();
                        Resources.Styles.NotifierClass notifierClass = new Resources.Styles.NotifierClass();
                        if (db.BasketGoods.Where(arg => arg.IdGood == allLaptop.Id).FirstOrDefault() != null)
                        {
                            notifierClass.notifier.ShowError("Ноутбук уже додано в корзину!");
                            return;
                        }    
                        basketGood.IdGood = allLaptop.Id;
                        basketGood.Quantity = 1;
                        basketGood.SalePrice = allLaptop.SalePrice;
                        db.BasketGoods.Add(basketGood);
                        db.SaveChanges();
                        //ShowLaptopFunction();
                        notifierClass.notifier.ShowInformation("Ноутбук додано в корзину");
                    }));
            }
        }
        public RelayCommand DeleteLaptopCommand
        {
            get
            {
                return deleteLaptopCommand ??
                    (deleteLaptopCommand = new RelayCommand((parametr) =>
                    {
                        Model.AllLaptop allLaptop = parametr as Model.AllLaptop;
                        if (allLaptop == null)
                            return;
                        MessageBoxResult messageBoxResult = View.MessageBox.Show("Ви бажаєте видалити "+ allLaptop.Name +"?","",MessageBoxButton.YesNo,"Так","Ні", null);
                        if (messageBoxResult == MessageBoxResult.Yes)
                        {
                            Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == allLaptop.Id).FirstOrDefault();
                            good.Deleted = 1;
                            good.Quantity = 0;
                            db.Entry(good).State = EntityState.Modified;
                            if (good.SN == 1)
                            {
                                Model.DBTable.SerialNumber serialNumber = db.SerialNumbers.Where(arg => arg.IdGood == good.Id).FirstOrDefault();
                                db.Entry(serialNumber).State = EntityState.Deleted;
                            }
                            db.SaveChanges();
                            ShowLaptopFunction();
                        }
                    }));
            }
        }
        public RelayCommand FilterComboBoxDropDownClosedCommand
        {
            get
            {
                return filterComboBoxDropDownClosedCommand ??
                    (filterComboBoxDropDownClosedCommand = new RelayCommand((parametr) =>
                    {
                        ComboBox comboBox = parametr as ComboBox;
                        if (comboBox == null)
                            return;
                        filterParam = comboBox.Text;
                        ShowLaptopFunction();
                    }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
