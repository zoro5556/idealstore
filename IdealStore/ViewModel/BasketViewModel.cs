﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Data.SQLite;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using System.Windows.Shapes;
using DevExpress.Mvvm.Native;


namespace IdealStore.ViewModel
{
    public class YourConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return values.Clone();
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    /*public class BrushColorConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (value.ToString().Length > 0)
                    return Brushes.Green;
                return Brushes.Green;
            }
            return Brushes.Yellow;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                if ((bool)value == true)
                    return Brushes.Yellow;
                else
                    return Brushes.Blue;
            }
            return Brushes.Blue;
        }
    }*/


    public class BasketViewModel : DependencyObject, INotifyPropertyChanged
    {
        ApplicationContext db;
        RelayCommand quanMinusCommand;
        RelayCommand quanPlusCommand;
        RelayCommand basketDeleteCommand;
        RelayCommand basketClearCommand;
        RelayCommand basketSaleCommand;
        RelayCommand basketRefreshCommand;
        RelayCommand addSNSaleCommand;
        RelayCommand changePriceCommand;
        RelayCommand moveToNextSNCommand;

        IEnumerable<Model.DBTable.Good> goods;
        IEnumerable<Model.DBTable.BasketGood> basketGoods;
        IEnumerable<AllBaskGood> allBaskGoods;
        IEnumerable<Model.DBTable.SerialNumber> serialNumbers;


        private List<Model.ColorChanged> snComboBoxBackColor;
        public List<Model.ColorChanged> SNComboBoxBackColor
        {
            get { return snComboBoxBackColor; }
            set
            {
                snComboBoxBackColor = value;
                OnPropertyChanged("SNComboBoxBackColor");
            }
        }

        private List<Model.DBTable.SerialNumber> serialNumbersList;
        public List<Model.DBTable.SerialNumber> SerialNumbersList
        {
            get { return serialNumbersList; }
            set
            {
                serialNumbersList = value;
                OnPropertyChanged("SerialNumbersList");
            }
        }
        public IEnumerable<Model.DBTable.BasketGood> BasketGoods
        {
            get { return basketGoods; }
            set
            {
                basketGoods = value;
                OnPropertyChanged("BasketGoods");
            }
        }
        public IEnumerable<Model.DBTable.Good> Goods
        {
            get { return goods; }
            set
            {
                goods = value;
                OnPropertyChanged("Goods");
            }
        }
        public IEnumerable<AllBaskGood> AllBaskGoods
        {
            get { return allBaskGoods; }
            set
            {
                allBaskGoods = value;
                OnPropertyChanged("AllBaskGoods");
            }
        }
        IEnumerable<Model.DBTable.SerialNumber> SerialNumbers
        {
            get { return serialNumbers; }
            set
            {
                serialNumbers = value;
                OnPropertyChanged("SerialNumbers");
            }
        }
        private int totalSum;
        public int TotalSum
        {
            get { return totalSum; }
            set
            {
                totalSum = value;
                OnPropertyChanged("TotalSum");
            }
        }

        //------------------------------------------------------
        public BasketViewModel()
        {
            AllBaskFunction();
        }

        //------------------------------------------------------

        public void AllBaskFunction()
        {
            db = new ApplicationContext();
            AllBaskGoods = (from b in db.BasketGoods
                     join g in db.Goods
                     on b.IdGood equals g.Id
                     select new AllBaskGood
                     {
                         Id = b.Id,
                         Name = g.Name,
                         SalePrice = b.SalePrice,
                         IdGood = b.IdGood,
                         MaxQuantity = g.Quantity,
                         Quantity = b.Quantity
                     }).ToList();
            TotalSum = 0;
            foreach (var allBaskGood in AllBaskGoods)
            {
                if (allBaskGood.Quantity > 0)
                for (int i = 0; i < allBaskGood.Quantity; i++ )
                TotalSum += allBaskGood.SalePrice;
            }
        }

        public RelayCommand MoveToNextSNCommand
        {
            get
            {
                return moveToNextSNCommand ??
                    (moveToNextSNCommand = new RelayCommand((parametr) =>
                    {
                    }));
            }
        }

        


        public RelayCommand ChangePriceCommand
        {
            get
            {
                return changePriceCommand ??
                    (changePriceCommand = new RelayCommand((parametr) =>
                    {
                        /*var values = parametr as object[];
                        View.Pages.BasketPage basketPage1 = values[0] as View.Pages.BasketPage;
                        Keyboard.ClearFocus();*/
                        View.Pages.BasketPage basketPage = parametr as View.Pages.BasketPage;

                        Keyboard.ClearFocus();
                        /*void OnExecute(object parameter)
                        {
                            var values = (object[])parameter;
                            var width = (double)values[0];
                            var height = (double)values[1];
                        }*/
                        if (basketPage != null)
                        {

                            AllBaskGood allBaskGood = basketPage.DataContext as AllBaskGood;
                            if (allBaskGood != null)
                            {
                                MessageBoxResult messageBoxResult = View.MessageBox.Show("Підтвердіть зміну ціни " + allBaskGood.Name, "", MessageBoxButton.YesNo, "Підтвердити", "Скасувати", "");
                                Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == allBaskGood.IdGood).FirstOrDefault();
                                if (good == null)
                                    return;
                                Model.DBTable.BasketGood basketGood = db.BasketGoods.Where(arg => arg.Id == allBaskGood.Id).FirstOrDefault();
                                if (basketGood == null)
                                    return;
                                if (messageBoxResult == MessageBoxResult.Yes)
                                {
                                    if (allBaskGood.SalePrice < good.Price)
                                    {
                                        View.MessageBox.Show("Введена ціна не може бути нижчою за закупочну ціну.\n" +
                                            "Закупочна ціна " + good.Price + "грн.", "", MessageBoxButton.OK, "Окей", "", "");
                                        allBaskGood.SalePrice = basketGood.SalePrice;
                                    }
                                    else
                                    {
                                        basketGood.SalePrice = allBaskGood.SalePrice;
                                        db.Entry(basketGood).State = EntityState.Modified;

                                        db.SaveChanges();
                                    }
                                }
                                else
                                    allBaskGood.SalePrice = good.SalePrice;

                                TotalSum = AllBaskGoods.Select(arg => arg.SalePrice * arg.Quantity).Sum();
                                //TotalSum = AllBaskGoods.Select(arg => arg.SalePrice).Sum();
                            }
                        }
                    }));
            }
        }
        public RelayCommand QuanMinusCommand
        {
            get
            {
                return quanMinusCommand ??
                    (quanMinusCommand = new RelayCommand((selectedItem) =>
                    {
                        if (selectedItem == null) return;
                        AllBaskGood allBaskGood = selectedItem as AllBaskGood;
                        if (allBaskGood.Quantity > 0)
                        {
                            Model.DBTable.BasketGood basketGood = (from b in db.BasketGoods
                                                     where b.Id == allBaskGood.Id
                                                     select b).FirstOrDefault();
                            basketGood.Quantity = --(allBaskGood.Quantity);
                            db.Entry(basketGood).State = EntityState.Modified;
                            db.SaveChanges();
                            AllBaskFunction();
                        }
                    }
                ));
            }
        }
        public RelayCommand QuanPlusCommand
        {
            get
            {
                return quanPlusCommand ??
                    (quanPlusCommand = new RelayCommand((selectedItem) =>
                    {
                        if (selectedItem == null) return;
                        AllBaskGood allBaskGood = selectedItem as AllBaskGood;
                        int quanCheck = (from g in db.Goods
                                         where g.Id == allBaskGood.IdGood
                                         select g.Quantity).FirstOrDefault();
                        if (allBaskGood.Quantity >= quanCheck) return;
                        Model.DBTable.BasketGood basketGood = (from b in db.BasketGoods
                                                 where b.Id == allBaskGood.Id
                                                 select b).FirstOrDefault();
                        basketGood.Quantity = ++(allBaskGood.Quantity);
                        db.Entry(basketGood).State = EntityState.Modified;
                        db.SaveChanges();
                        AllBaskFunction();
                    }));
            }
        }
        public RelayCommand BasketDeleteCommand
        {
            get
            {
                return basketDeleteCommand ??
                    (basketDeleteCommand = new RelayCommand((selectedItem) =>
                    {
                        if (selectedItem == null) return;
                        AllBaskGood allBaskGood = selectedItem as AllBaskGood;
                        Model.DBTable.BasketGood basketGood = db.BasketGoods.Find(allBaskGood.Id);
                        db.BasketGoods.Remove(basketGood);
                        db.SaveChanges();
                        AllBaskFunction();
                    }));
            }
        }
        public RelayCommand BasketClearCommand
        {
            get
            {
                return basketClearCommand ??
                    (basketClearCommand = new RelayCommand((o) =>
                    {
                        if (AllBaskGoods != null)
                        {
                            BasketGoods = (from b in db.BasketGoods
                                           select b).ToList();
                            db.BasketGoods.RemoveRange(BasketGoods);
                            db.SaveChanges();
                            AllBaskFunction();
                        }
                    }));
            }
        }
        public RelayCommand BasketSaleCommand
        {
            get
            {
                return basketSaleCommand ??
                    (basketSaleCommand = new RelayCommand((o) =>
                    {
                        BasketGoods = (from b in db.BasketGoods
                                       select b).ToList();
                        if (BasketGoods.Count() == 0) 
                            return;
                        MessageBoxResult messageBoxResult = View.MessageBox.Show("Загальна сумма чеку " + TotalSum +
                            " грн.Виберіть спосіб оплати: ", "", MessageBoxButton.YesNo, "Готівковий", "Безготівковий", null);

                        if (messageBoxResult == MessageBoxResult.None)
                            return;
                        Model.DBTable.Transaction transaction = new Model.DBTable.Transaction();
                        transaction.Date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                        transaction.Sum = TotalSum;
                        if (messageBoxResult == MessageBoxResult.Yes)
                            transaction.Cash = 1;
                        if (messageBoxResult == MessageBoxResult.None && messageBoxResult == MessageBoxResult.No)
                            return;

                        db.Transactions.Add(transaction);
                        db.SaveChanges();

                        Model.DBTable.Good good = new Model.DBTable.Good();
                        List<Model.DBTable.BasketGood> SNGoods = new List<Model.DBTable.BasketGood>();

                        foreach (Model.DBTable.BasketGood basketGood in BasketGoods)
                        {
                            good = (from g in db.Goods
                                    where g.Id == basketGood.IdGood
                                    select g).FirstOrDefault();
                            if (good.GoodType == 29)
                            {
                                continue;
                            }
                            if (good.SN == 1)
                            {
                                SNGoods.Add(basketGood);
                            }
                        }

                        if (SNGoods.Count > 0)
                        {
                            SerialNumbersList = new List<Model.DBTable.SerialNumber>();

                            View.BasketView.SaleSNView saleSNView = new View.BasketView.SaleSNView();
                            
                            var ab = new BrushConverter();

                            for (int i = 0; i < SNGoods.Sum(arg => arg.Quantity); i++)
                            {
                                Model.DBTable.SerialNumber serial = new Model.DBTable.SerialNumber();
                                serial.Id = i;
                                SerialNumbersList.Add(serial);
                            }

                            saleSNView.DataContext = SerialNumbersList;

                            saleSNView.AddSNSaleButton.DataContext = this;
                            saleSNView.AddSNSaleButton.CommandParameter = saleSNView;

                            int idSN = 0;
                            SNComboBoxBackColor = new List<Model.ColorChanged>();

                            foreach (Model.DBTable.BasketGood SNgood in SNGoods)
                            {
                                SerialNumbers = (from s in db.SerialNumbers
                                                 where s.IdGood == SNgood.IdGood
                                                 where s.IdTransaction == 0 && s.IdComputer == 0
                                                 select s).ToList();
                                good = (from g in db.Goods
                                        where g.Id == SNgood.IdGood
                                        select g).FirstOrDefault();

                                var textBlock = new TextBlock() { Text = good.Name };

                                saleSNView.SNListContainer.Children.Add(new Rectangle());
                                saleSNView.SNListContainer.Children.Add(textBlock);
                                for (int i = 0; i < SNgood.Quantity; i++)
                                {
                                    //SNComboBoxBackColor[i] = (Brush)ab.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyLightGrayBrush")));
                                    Model.ColorChanged colorChanged = new Model.ColorChanged();
                                    colorChanged.IdColor = SerialNumbersList[idSN].Id;
                                    colorChanged.NameColor = (Brush)ab.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyLightGrayBrush"))); 
                                    SNComboBoxBackColor.Add(colorChanged);
                                    //SNComboBoxBackColor[i] = (Brush)ab.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyLightGrayBrush")));

                                    /*
                                     * 
                                     * Коректне додавання кольорів і біндинг до нього  
                                     * 
                                     * -Зробити трігер для перевірки дліни тексту. Остання вкладка хрому
                                     * -Перевірити щоб він працював при зміні тексту(якщо випадково серійний номер буде написаний)
                                     */

                                     var comboBox = new ComboBox() { DataContext = SerialNumbersList[idSN]};

                                    Binding binding = new Binding();
                                    binding.Source = this;
                                    binding.Path = new PropertyPath("SNComboBoxBackColor["+idSN+"].NameColor");
                                    binding.Mode = BindingMode.TwoWay;
                                    binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                                    comboBox.SetBinding(ComboBox.BorderBrushProperty, binding);

                                    comboBox.ItemsSource = SerialNumbers.Select(a => a.SN);
                                    saleSNView.SNListContainer.Children.Add(comboBox);
                                    idSN++;
                                }
                            }
                            if (saleSNView.ShowDialog() == true)
                            {
                                db.SaveChanges();
                                foreach (Model.DBTable.SerialNumber serialNumber in SerialNumbersList)
                                {
                                    Model.DBTable.SerialNumber addedSN = db.SerialNumbers.Where(arg => arg.SN == serialNumber.SN).FirstOrDefault();
                                    addedSN.IdTransaction = db.Transactions.OrderByDescending(arg => arg.Id).Select(arg => arg.Id).FirstOrDefault();
                                    db.Entry(addedSN).State = EntityState.Modified;
                                }
                            }
                            else
                            {
                                transaction = db.Transactions.OrderByDescending(arg => arg.Id).FirstOrDefault();
                                db.Entry(transaction).State = EntityState.Deleted;
                                db.SaveChanges();
                                return;
                            }
                        }


                        int IdTrans = (from t in db.Transactions
                                       orderby t.Id descending
                                       select t.Id).FirstOrDefault();
                        // перевірка, чи є товар з сн в конфігураторі
                        if (SerialNumbersList != null)
                        {
                            foreach (Model.DBTable.SerialNumber serialNumber in SerialNumbersList)
                            {
                                Model.DBTable.ConfiguratorComponent configuratorComponent = db.ConfiguratorComponents.Where(arg => arg.SN == serialNumber.SN).FirstOrDefault();
                                if (configuratorComponent != null)
                                    db.Entry(configuratorComponent).State = EntityState.Deleted;
                            }
                        }
                        foreach (Model.DBTable.BasketGood basketGood in BasketGoods)
                        {
                            // перевірка, чи є товар в конфігураторі
                            List<Model.DBTable.ConfiguratorComponent> ConfiguratorComponents = db.ConfiguratorComponents.Where(arg => arg.IdGood == basketGood.IdGood).ToList();
                            good = db.Goods.Where(arg => arg.Id == basketGood.IdGood).FirstOrDefault();
                            if (ConfiguratorComponents.Count > 0)
                            {
                                if (good.SN == 0)
                                {
                                    if ((ConfiguratorComponents.Count + basketGood.Quantity) > good.Quantity)
                                    {
                                        int CountDelConfGood = basketGood.Quantity - (good.Quantity - ConfiguratorComponents.Count);
                                        for(int i = 0; i < CountDelConfGood; i++)
                                        {
                                            db.Entry(db.ConfiguratorComponents.Where(arg => arg.IdGood == basketGood.IdGood).FirstOrDefault()).State = EntityState.Deleted;
                                        }
                                    }
                                }
                            }
                            
                            //
                            Model.DBTable.Sale sale = new Model.DBTable.Sale();
                            
                            good.Quantity = good.Quantity - basketGood.Quantity;
                            sale.IdTransaction = IdTrans;
                            sale.IdGood = basketGood.IdGood;
                            sale.Quantity = basketGood.Quantity;
                            sale.SalePrice = basketGood.SalePrice;
                            db.Sales.Add(sale);
                            db.BasketGoods.Remove(basketGood);
                            if (good.GoodType == 29)
                            {
                                Model.DBTable.SerialNumber serialNumber = db.SerialNumbers.Where(arg => arg.IdGood == good.Id).FirstOrDefault();
                                serialNumber.IdTransaction = IdTrans;
                                db.Entry(serialNumber).State = EntityState.Modified;
                                good.Quantity = 0;
                            }
                            db.Entry(good).State = EntityState.Modified;
                        }
                        db.SaveChanges();
                        AllBaskFunction();
                    }));
            }
        }
        public RelayCommand BasketRefreshCommand
        {
            get
            {
                return basketRefreshCommand ??
                    (basketRefreshCommand = new RelayCommand((o) =>
                    {
                        AllBaskFunction();
                    }
                    ));
            }
        }

        public RelayCommand AddSNSaleCommand
        {
            get
            {
                return addSNSaleCommand ??
                    (addSNSaleCommand = new RelayCommand((o) =>
                    {
                        var saleSNView = o as View.BasketView.SaleSNView;

                        var ab = new BrushConverter();
                        bool addError = false;

                        for (int i = 0; i < SNComboBoxBackColor.Count; i++)
                            SNComboBoxBackColor[i].NameColor = (Brush)ab.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyLightGrayBrush")));
                        /*for (int i = 0; i < SerialNumbersList.Count; i++)
                        {
                            Model.DBTable.SerialNumber serialNumber = SerialNumbersList[i] as Model.DBTable.SerialNumber;
                            int idCheck = db.SerialNumbers.Where(arg => arg.SN == serialNumber.SN).Select(arg => arg.Id).FirstOrDefault();
                            if (idCheck == 0 || serialNumber.SN == null || serialNumber.SN == string.Empty)
                            {
                                //SNComboBoxBackColor = SNComboBoxBackColor;
                                SNComboBoxBackColor[i].NameColor = Brushes.Red;
                                addError = true;
                            }
                        }*/
                        foreach(Model.DBTable.SerialNumber serialNumber in SerialNumbersList)
                        {
                            Model.DBTable.SerialNumber snCheck = db.SerialNumbers.Where(arg => arg.SN == serialNumber.SN).FirstOrDefault();
                            if (serialNumber == null || serialNumber.SN == string.Empty || snCheck == null)
                            {
                                Model.ColorChanged colorChanged = SNComboBoxBackColor.Where(arg => arg.IdColor == serialNumber.Id).FirstOrDefault();
                                colorChanged.NameColor = Brushes.Red;
                                addError = true;
                            }
                        }
                        List<string> SNGroupStrings = SerialNumbersList.GroupBy(arg => arg.SN).Select(arg => arg).Select(arg => arg.Key).ToList();
                        if (SNGroupStrings.Count > 0 && SNGroupStrings.Count != SerialNumbersList.Count())
                        {
                            foreach(string snGroupString in SNGroupStrings)
                            {
                                if (SerialNumbersList.Where(arg => arg.SN == snGroupString).Count() > 1)
                                {
                                    List<int> IdGroupList = SerialNumbersList.Where(arg => arg.SN == snGroupString).Select(arg => arg.Id).ToList();
                                    foreach (int idGroup in IdGroupList)
                                        SNComboBoxBackColor.Where(arg => arg.IdColor == idGroup).ToList().ForEach(arg => arg.NameColor = Brushes.Red);
                                    addError = true;
                                }
                            }
                        }
                        /*if (SNGroupString.Count > 0 && SNGroupString.Count != SerialNumbersList.Count())
                        {
                            for (int i = 0; i < SNGroupString.Count; i++)
                            {
                                List<Model.DBTable.SerialNumber> SNGroupList = new List<Model.DBTable.SerialNumber>();
                                SNGroupList = SerialNumbersList.Where(arg => arg.SN == Convert.ToString(SNGroupString[i].Key)).ToList();
                                if (SNGroupList.Count() > 1)
                                    for (int a = 0; a < SNGroupList.Count; i++)
                                        SNComboBoxBackColor[a].NameColor = Brushes.Red;

                            }
                            addError = true;
                        }*/
                        
                        SNComboBoxBackColor = SNComboBoxBackColor;
                        if (addError == false)
                            saleSNView.DialogResult = true;
                        else
                            View.MessageBox.Show("Перевірьте правельність введених даних.\nСерійні номера повинні бути різні і бути вибранні із списку.", "", MessageBoxButton.OK, "Окей", null, null);
                    }
                    ));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
