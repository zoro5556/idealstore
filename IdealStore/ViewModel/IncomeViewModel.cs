﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Data.SQLite;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using System.Windows.Shapes;
using DevExpress.Mvvm.Native;
using IdealStore.Model;
using System.Windows.Documents;
using Microsoft.Xaml.Behaviors;
using ToastNotifications.Utilities;
using IdealStore.Model.DBTable;
using Xceed.Wpf.Toolkit.Converters;

namespace IdealStore.ViewModel
{
    public class IncomeViewModel : INotifyPropertyChanged
    {
        public IncomeViewModel()
        {
            
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
