﻿using DevExpress.Mvvm.Native;
using IdealStore.Model;
using IdealStore.Model.DBTable;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ToastNotifications.Utilities;

namespace IdealStore.ViewModel
{
    public class ConfiguratorViewModel : INotifyPropertyChanged
    {
        ApplicationContext db;
        RelayCommand addNewComponentCommand;
        RelayCommand acceptSelectCompViewCommand;
        RelayCommand focusOnTextBoxCommand;
        RelayCommand selectItemCommand;
        RelayCommand removeComponentCommand;
        RelayCommand changeSelectedItemCommand;
        RelayCommand allComponentClearCommand;
        RelayCommand addNewComputerCommand;
        RelayCommand addNewComponentTypeCommand;
        //RelayCommand acceptSelectCompTypeCommand;
        RelayCommand compTypeComboBoxDropDownClosedCommand;
        RelayCommand acceptChangeTotalSumCompViewCommand;
        private ObservableCollection<Model.AllComponent> allComponents;
        private List<Model.AllGood> allGoods;
        private IEnumerable<Model.DBTable.Computer> computers;
        private IEnumerable<Model.DBTable.Component> components;
        private IEnumerable<Model.DBTable.ConfiguratorComponent> configuratorComponents;
        private string scanningBarCode;
        private int totalComponentPrice;
        private string compSN;
        private bool usedComputer_IsChecked;
        private int count;
        private int totalPCSum;
        int desiredEarningsSum; // заробіток з комп'ютера
        int[] idMandatoryComponents = new int[] { 10, 11, 17, 18}; //ід обов'язкових комплектуючих
        int[] idType = { 10, 11, 12, 14, 15, 16, 17, 18, 27, 28 }; //ід типу комплектуючих
        int[] idRepeatingСomponents = {10, 27, 28};
        BrushConverter brushConverter = new BrushConverter();

        public int DesiredEarningsSum
        {
            get { return desiredEarningsSum; }
            set
            {
                desiredEarningsSum = value;
                TotalPCSum = value + totalComponentPrice;
                OnPropertyChanged("DesiredEarningsSum");
            }
        }
        public int TotalPCSum
        {
            get { return totalPCSum; }
            set
            {
                totalPCSum = value;
                OnPropertyChanged("TotalPCSum");
            }
        }
        public int Count
        {
            get { return count; }
            set
            {
                count = value;
                OnPropertyChanged("Count");
            }
        }
        public bool UsedComputer_IsChecked
        {
            get { return usedComputer_IsChecked; }
            set
            {
                usedComputer_IsChecked = value;
                OnPropertyChanged("UsedComputer_IsChecked");
            }
        }
        public string CompSN
        {
            get { return compSN; }
            set
            {
                compSN = value;
                OnPropertyChanged("CompSN");
            }
        }
        public int TotalComponentPrice
        {
            get { return totalComponentPrice; }
            set
            {
                totalComponentPrice = value;
                OnPropertyChanged("TotalComponentPrice");
            }
        }
        public string ScanningBarCode
        {
            get { return scanningBarCode; }
            set
            {
                scanningBarCode = value;
                OnPropertyChanged("ScanningBarCode");
            }
        }
        public List<Model.AllGood> AllGoods
        {
            get { return allGoods; }
            set
            {
                allGoods = value;
                OnPropertyChanged("AllGoods");
            }
        }
        public ObservableCollection<Model.AllComponent> AllComponents
        {
            get { return allComponents; }
            set
            {
                allComponents = value;
                OnPropertyChanged("AllComponents");
            }
        }
        public IEnumerable<Model.DBTable.Computer> Computers
        {
            get { return computers; }
            set
            {
                computers = value;
                OnPropertyChanged("Computers");
            }
        }
        public IEnumerable<Model.DBTable.Component> Components
        {
            get { return components; }
            set
            {
                components = value;
                OnPropertyChanged("Components");
            }
        }
        public IEnumerable<Model.DBTable.ConfiguratorComponent> ConfiguratorComponents
        {
            get { return configuratorComponents; }
            set
            {
                configuratorComponents = value;
                OnPropertyChanged("ConfiguratorComponents");
            }
        }

        public void ShowComponentsFunction()
        {
            List<Model.DBTable.ConfiguratorComponent> SelectedConfiguratorComponents = db.ConfiguratorComponents.ToList();
            AllComponents = new ObservableCollection<Model.AllComponent>();
            int i = 1;
            Model.AllComponent component;
            foreach (int id in idType)
            {
                component = new Model.AllComponent();
                component.Id = i++;
                component.CompType = db.TypeGoods.Where(arg => arg.Id == id).Select(arg => arg.GoodType).FirstOrDefault();
                Model.DBTable.ConfiguratorComponent configuratorComponent = SelectedConfiguratorComponents.Where(arg => (db.Goods.Where(arg1 => arg1.Id == arg.IdGood).FirstOrDefault().GoodType) == id).FirstOrDefault();
                if (configuratorComponent != null)
                {
                    Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == configuratorComponent.IdGood).FirstOrDefault();
                    component.IdGood = good.Id;
                    component.Name = good.Name;
                    component.SN = configuratorComponent.SN;
                    component.Price = good.SalePrice;
                    TotalComponentPrice += component.Price;
                    SelectedConfiguratorComponents.Remove(configuratorComponent);
                    // якщо серійний номер був, поки закрили сторінку (користувач зібрав ПК, але не додав його, перейшов в товари і продав Товар з серійним номером уже вибраним)
                    //коли додається іще одна комплектуюча із новим списком.
                }
                AllComponents.Add(component);
            }
            if (SelectedConfiguratorComponents.Count > 0)
            {
                foreach (Model.DBTable.ConfiguratorComponent configuratorComponent in SelectedConfiguratorComponents)
                {
                    component = new Model.AllComponent();
                    Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == configuratorComponent.IdGood).FirstOrDefault();
                    component.Id = i++;
                    component.CompType = db.TypeGoods.Where(arg => arg.Id == good.GoodType).Select(arg => arg.GoodType).FirstOrDefault();
                    component.IdGood = good.Id;
                    component.Name = good.Name;
                    component.Price = good.SalePrice;
                    TotalComponentPrice += component.Price;
                    AllComponents.Add(component);
                }
            }
            AllComponents.Add(new Model.AllComponent() { Id = AllComponents.Count() });
        }
        
        public ConfiguratorViewModel()
        {
            db = new ApplicationContext();
            ShowComponentsFunction();
        }


        public RelayCommand AcceptChangeTotalSumCompViewCommand
        {
            get
            {
                return acceptChangeTotalSumCompViewCommand ??
                    (acceptChangeTotalSumCompViewCommand = new RelayCommand((parametr) =>
                    {
                        View.ConfiguratorView.ChangeComputerSumView changeComputerSumView = parametr as View.ConfiguratorView.ChangeComputerSumView;
                        changeComputerSumView.DialogResult = true;
                    }));
            }
        }
        public RelayCommand CompTypeComboBoxDropDownClosedCommand
        {
            get
            {
                return compTypeComboBoxDropDownClosedCommand ??
                    (compTypeComboBoxDropDownClosedCommand = new RelayCommand((parametr) =>
                    {
                        View.Pages.ConfiguratorPage configuratorPage = parametr as View.Pages.ConfiguratorPage;
                        ListBoxItem listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(configuratorPage.componentsList.Items.Count - 1));
                        ComboBox comboBox = listBoxItem.FindChild<ComboBox>("CompTypeComboBox");
                        Button button = listBoxItem.FindChild<Button>("AddNewCompTypeButton");
                        if(comboBox.Text.Length == 0)
                        {
                            MessageBoxResult messageBoxResult = View.MessageBox.Show("Не вибраний тип комплектуючої. Ви бажаєте відмінити вибір?", "", MessageBoxButton.YesNo, "Так", "Ні", null);
                            if (messageBoxResult == MessageBoxResult.Yes)
                            {
                                comboBox.Visibility = Visibility.Collapsed;
                                button.IsEnabled = true;
                            }
                            if(messageBoxResult == MessageBoxResult.No || messageBoxResult == MessageBoxResult.None)
                            {
                                comboBox.IsDropDownOpen = true;
                            }
                            return;
                        }
                        else
                        {
                            comboBox.Visibility = Visibility.Collapsed;
                            AllComponents.LastOrDefault().CompType = comboBox.Text;
                            AllComponents.Add(new AllComponent() { Id = AllComponents.Count()});
                        }
                    }));
            }
        }
        /*public RelayCommand AcceptSelectCompTypeCommand
        {
            get
            {
                return acceptSelectCompTypeCommand ??
                    (addNewComponentCommand = new RelayCommand((parametr) =>
                    {
                        View.ConfiguratorView.AddNewCompTypeView addNewCompTypeView = parametr as View.ConfiguratorView.AddNewCompTypeView;
                        if (addNewCompTypeView == null)
                            return;
                        if (addNewCompTypeView.TypeSelectComboBox.Text.Length > 0)
                            addNewCompTypeView.DialogResult = true;
                        else
                            View.MessageBox.Show("Ви не вибрали жодного типу","",MessageBoxButton.OK, "Повторити",null, null);
                    }));
            }
        }*/
        public RelayCommand AddNewComponentTypeCommand
        {
            get
            {
                return addNewComponentTypeCommand ??
                    (addNewComponentTypeCommand = new RelayCommand((parametr) =>
                    {
                        View.Pages.ConfiguratorPage configuratorPage = parametr as View.Pages.ConfiguratorPage;
                        ListBoxItem listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(configuratorPage.componentsList.Items.Count-1));
                        ComboBox comboBox = listBoxItem.FindChild<ComboBox>("CompTypeComboBox");
                        comboBox.Visibility = Visibility.Visible;
                        comboBox.IsDropDownOpen = true;
                        comboBox.Items.Clear();
                        foreach (int id in idRepeatingСomponents)
                        {
                            string typeComp = db.TypeGoods.Where(arg => arg.Id == id).Select(arg => arg.GoodType).FirstOrDefault();
                            switch (typeComp)
                            {
                                case "Оперативна пам'ять":
                                    if (AllComponents.Where(arg => arg.CompType == typeComp).Count() < 4)
                                        comboBox.Items.Add(typeComp);
                                    break;
                                case "HDD":
                                case "SSD":
                                    if (AllComponents.Where(arg => arg.CompType == typeComp).Count() < 3)
                                        comboBox.Items.Add(typeComp);
                                    break;
                            }
                        }

                        Button button = listBoxItem.FindChild<Button>("AddNewCompTypeButton");
                        button.IsEnabled = false;
                    }));
            }
        }
        public RelayCommand AddNewComputerCommand
        {
            get
            {
                return addNewComputerCommand ??
                    (addNewComputerCommand = new RelayCommand((parametr) =>
                    {
                        if (db.BasketGoods.Count() > 0)
                        {
                            View.MessageBox.Show("Для додавання комп'ютера необхідно очистити вміст кошика.\n Продайте або очистіть корзину.","",MessageBoxButton.OK, "Окей", null, null);
                            //List<Model.DBTable.ConfiguratorComponent>
                            return;
                        }
                        View.Pages.ConfiguratorPage configuratorPage = parametr as View.Pages.ConfiguratorPage;
                        bool addComputer = true;
                        foreach (int idMandatoryComponent in idMandatoryComponents)
                        {
                            Model.AllComponent allComponent = AllComponents.Where(arg => arg.CompType == db.TypeGoods.Where(argum => argum.Id == idMandatoryComponent)
                            .Select(argum => argum.GoodType).FirstOrDefault()).FirstOrDefault();
                            if (allComponent != null && allComponent.IdGood == 0)
                            {
                                addComputer = false;
                                ListBoxItem listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(allComponent.Id-1));
                                Rectangle rectangle = listBoxItem.FindChild<Rectangle>("UnderlineRectangle");
                                rectangle.Fill = Brushes.Red;
                                Ellipse ellipse = listBoxItem.FindChild<Ellipse>(string.Empty);
                                ellipse.Visibility = Visibility.Visible;
                                                               
                                listBoxItem.ToolTip = new ToolTip() { Content = "Поле обов'язкове для вибору"};
                            }
                        }
                        List<Model.AllComponent> HDDorSSDCheck = AllComponents.Where(arg => arg.CompType == "HDD" || arg.CompType == "SSD").ToList();
                        if (HDDorSSDCheck != null)
                        {
                            bool checkHDDorSSD = false;
                            foreach (Model.AllComponent allComponent in HDDorSSDCheck)
                            {
                                if (allComponent.IdGood > 0)
                                {
                                    checkHDDorSSD = true;
                                    break;
                                }
                            }
                            if (checkHDDorSSD == true)
                            {
                                if (addComputer == true)
                                    addComputer = true;
                            }
                            else
                            {
                                addComputer = false;
                                foreach (Model.AllComponent allComponent in HDDorSSDCheck)
                                {
                                    ListBoxItem listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(allComponent.Id-1));
                                    //listBoxItem.BorderThickness = new Thickness(1);
                                    //listBoxItem.BorderBrush = Brushes.Red;
                                    Rectangle rectangle = listBoxItem.FindChild<Rectangle>("UnderlineRectangle");
                                    rectangle.Fill = Brushes.Red;
                                    Ellipse ellipse = listBoxItem.FindChild<Ellipse>(string.Empty);
                                    ellipse.Visibility = Visibility.Visible;

                                    listBoxItem.ToolTip = new ToolTip() { Content = "Поле обов'язкове для вибору" };
                                }
                            }
                        }
                        if (addComputer == true)
                        {
                            DesiredEarningsSum = 1500;
                            TotalPCSum = TotalComponentPrice + DesiredEarningsSum;
                            MessageBoxResult messageBoxResult = View.MessageBox.Show("Загальна сумма за комп'ютер " +  TotalPCSum +" грн.","",MessageBoxButton.YesNoCancel, "Додати", "Змінити ціну", " Скасувати");

                            Model.DBTable.Computer computer = new Model.DBTable.Computer();
                            switch (messageBoxResult)
                            {
                                case MessageBoxResult.Yes:
                                    computer.TotalPcPrice = TotalPCSum;
                                    break;
                                case MessageBoxResult.No:
                                    View.ConfiguratorView.ChangeComputerSumView changeComputerSumView = new View.ConfiguratorView.ChangeComputerSumView();
                                    changeComputerSumView.DataContext = this;
                                    if (changeComputerSumView.ShowDialog() == true)
                                    {
                                        computer.TotalPcPrice = DesiredEarningsSum + TotalComponentPrice; 
                                        break;
                                    }
                                    else
                                        return;
                                case MessageBoxResult.Cancel:
                                case MessageBoxResult.None:
                                    return;
                            }
                            computer.SaleDate = string.Empty;
                            if (UsedComputer_IsChecked == true)
                                computer.Used = 1;
                            computer.PCName = "Персональний комп'ютер " + (db.Computers.Count() + 1);
                            db.Computers.Add(computer);
                            db.SaveChanges();
                            int idComputer = db.Computers.OrderByDescending(arg => arg.Id).Select(arg => arg.Id).FirstOrDefault();
                            foreach (Model.AllComponent allComponent in AllComponents.Where(arg => arg.IdGood > 0))
                            {
                                Model.DBTable.Component component = new Model.DBTable.Component();
                                component.IdComputer = idComputer;
                                component.IdGood = allComponent.IdGood;
                                component.IdSN = db.SerialNumbers.Where(arg => arg.SN == allComponent.SN).Select(arg => arg.Id).FirstOrDefault();
                                component.Price = allComponent.Price;
                                component.IdType = db.TypeGoods.Where(arg => arg.GoodType == allComponent.CompType).Select(arg => arg.Id).FirstOrDefault();
                                db.Components.Add(component);
                                Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == allComponent.IdGood).FirstOrDefault();
                                good.Quantity--; 
                                db.Entry(good).State = EntityState.Modified;
                                if (component.IdSN > 0)
                                {
                                    Model.DBTable.SerialNumber serialNumber = db.SerialNumbers.Where(arg => arg.Id == component.IdSN).FirstOrDefault();
                                    serialNumber.IdComputer = idComputer;
                                    db.Entry(serialNumber).State = EntityState.Modified;
                                }
                            }
                            /*Перевірка на товари і очистка з корзиниforeach (Model.DBTable.ConfiguratorComponent configuratorComponent in db.ConfiguratorComponents.GroupBy(arg => arg.IdGood))
                            {
                                Model.DBTable.BasketGood basketGood = db.BasketGoods.Where(arg => arg.IdGood == configuratorComponent.IdGood).FirstOrDefault();
                                if (basketGood != null)
                                {
                                    Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == configuratorComponent.IdGood).FirstOrDefault();
                                    if ((db.ConfiguratorComponents.Where(arg => arg.IdGood == good.Id).Count() + basketGood.Quantity) > good.Quantity)
                                    {
                                        basketGood.Quantity = good.Quantity - (db.ConfiguratorComponents.Where(arg => arg.IdGood == good.Id).Count());
                                        db.Entry(basketGood).State = EntityState.Modified;
                                    }
                                }
                            }*/
                            db.ConfiguratorComponents.RemoveRange(db.ConfiguratorComponents.ToList());
                            db.SaveChanges();

                            /*for (int i = 0; i < configuratorPage.componentsList.Items.Count; i++)
                            {
                                ListBoxItem listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(i));
                                Button button = listBoxItem.FindChild<Button>(string.Empty);
                                if (button.Visibility == Visibility.Collapsed)
                                {
                                    button.Visibility = Visibility.Visible;
                                    TextBlock textBlock = listBoxItem.FindChild<TextBlock>("NameText");
                                    textBlock.Visibility = Visibility.Collapsed;
                                    AllComponents[i].Name = null;
                                    AllComponents[i].Price = 0;
                                    AllComponents[i].SN = null;
                                    AllComponents[i].IdGood = 0;
                                    TotalComponentPrice = 0;
                                    Button delButton = listBoxItem.FindChild<Button>("DelCompButton");
                                    delButton.Visibility = Visibility.Collapsed;
                                }
                            }*/
                            AllComponents = null;
                            ShowComponentsFunction();
                            UsedComputer_IsChecked = false;
                            TotalComponentPrice = 0;
                        }    
                    }));
            }
        }
        public RelayCommand AllComponentClearCommand
        {
            get
            {
                return allComponentClearCommand ??
                    (allComponentClearCommand = new RelayCommand((parametr) =>
                    {
                        View.Pages.ConfiguratorPage configuratorPage = parametr as View.Pages.ConfiguratorPage;
                        MessageBoxResult messageBoxResult = View.MessageBox.Show("Ви точно бажаєте очистити список комплектуючих?","", MessageBoxButton.YesNo, "Так", "Ні", null);
                        if (messageBoxResult == MessageBoxResult.Yes)
                        {
                            AllComponents = AllComponents;
                            for(int i = 0; i < AllComponents.Count; i++)
                            {
                                if (AllComponents[i].Name != null && AllComponents[i].Name.Length > 0)
                                {
                                    ListBoxItem listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(i));
                                    AllComponents[i].Name = null;
                                    AllComponents[i].IdGood = 0;
                                    AllComponents[i].Price = 0;
                                    AllComponents[i].SN = null;
                                    TotalComponentPrice = 0;
                                }
                            }
                            db.ConfiguratorComponents.RemoveRange(db.ConfiguratorComponents.ToList());
                            db.SaveChanges();
                            /*for (int i = 0; i < configuratorPage.componentsList.Items.Count; i++)
                            {
                                ListBoxItem listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(i));
                                Button button = listBoxItem.FindChild<Button>("AddNewCompButton");
                                if (button.Visibility == Visibility.Collapsed)
                                {
                                    //button.Visibility = Visibility.Visible;
                                    TextBlock textBlock = listBoxItem.FindChild<TextBlock>("NameText");
                                    textBlock.Visibility = Visibility.Collapsed;
                                    AllComponents[i].Name = null;
                                    AllComponents[i].Price = 0;
                                    AllComponents[i].SN = null;
                                    AllComponents[i].IdGood = 0;
                                    TotalComponentPrice = 0;
                                    // При видаленні всіх компонетів не прибирається ДелБатон з нових доданих

                                    Button delButton;
                                    delButton = listBoxItem.FindChild<Button>("DelCompButton");
                                    delButton.Visibility = Visibility.Collapsed;
                                }
                            }*/
                        }
                    }));
            }
        }
        public RelayCommand ChangeSelectedItemCommand
        {
            get
            {
                return changeSelectedItemCommand ??
                    (changeSelectedItemCommand = new RelayCommand((parametr) =>
                    {
                        View.ConfiguratorView.SelectCompView selectCompView = parametr as View.ConfiguratorView.SelectCompView; 
                        Model.AllGood allGood = selectCompView.selectedCompList.SelectedItem as Model.AllGood;
                        if (allGood == null)
                            return;
                        if (allGood.SN == 1)
                        {
                            CompSN = null;
                            selectCompView.SNStackPanel.Visibility = Visibility.Visible;
                            List<Model.DBTable.SerialNumber> serialNumbers = (from s in db.SerialNumbers
                                                                              where s.IdGood == allGood.Id
                                                                              where s.IdTransaction == 0 && s.IdComputer == 0
                                                                              select s).ToList();
                            foreach (string snRemove in AllComponents.Select(arg => arg.SN))
                                serialNumbers.Remove(serialNumbers.Where(arg => arg.SN == snRemove).FirstOrDefault());
                            selectCompView.SNCompComboBox.Items.Clear();
                            foreach (Model.DBTable.SerialNumber serialNumber in serialNumbers)
                            {
                                selectCompView.SNCompComboBox.Items.Add(serialNumber.SN);
                            }
                        }
                    }));
            }
        }
        public RelayCommand RemoveComponentCommand
        {
            get
            {
                return removeComponentCommand ??
                    (removeComponentCommand = new RelayCommand((parametr) =>
                    {
                        View.Pages.ConfiguratorPage configuratorPage = parametr as View.Pages.ConfiguratorPage;
                        ListBoxItem listBoxItem = new ListBoxItem();
                        for (int i = 0; i < configuratorPage.componentsList.Items.Count; i++)
                        {
                            listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(i));
                            if (listBoxItem.IsMouseOver == true)
                                break;
                        }
                        if (listBoxItem != null)
                        {
                            Model.AllComponent allComponent = listBoxItem.DataContext as Model.AllComponent;
                            /*Button button = listBoxItem.FindChild<Button>(string.Empty);
                            button.Visibility = Visibility.Visible;*/
                            /*TextBlock textBlock = listBoxItem.FindChild<TextBlock>("NameText");
                            textBlock.Visibility = Visibility.Collapsed;*/
                            Model.DBTable.ConfiguratorComponent configuratorComponent = db.ConfiguratorComponents.Where(arg => arg.IdGood == allComponent.IdGood && arg.SN == allComponent.SN).FirstOrDefault();
                            db.Entry(configuratorComponent).State = EntityState.Deleted;
                            db.SaveChanges();
                            allComponent.Name = null;
                            TotalComponentPrice -= allComponent.Price;
                            allComponent.Price = 0;
                            allComponent.IdGood = 0;
                            allComponent.SN = null;
                            /*Button delButton = listBoxItem.FindChild<Button>("DelCompButton");
                            delButton.Visibility = Visibility.Collapsed;*/
                            //AllComponents[allComponent.Id] = allComponent;
                        }
                    }));
            }
        }
        public RelayCommand SelectItemCommand
        {
            get
            {
                return selectItemCommand ??
                    (selectItemCommand = new RelayCommand((parametr) =>
                    {
                        View.ConfiguratorView.SelectCompView selectCompView = parametr as View.ConfiguratorView.SelectCompView;
                        if (selectCompView.SNCompComboBox.IsKeyboardFocusWithin == true)
                        {
                            return;
                        }
                        Model.AllGood allGood = AllGoods.Where(arg => arg.Barcode == ScanningBarCode).FirstOrDefault();
                        selectCompView.BarCodeTextBox.Visibility = Visibility.Collapsed;
                        if (allGood != null)
                        {
                            selectCompView.selectedCompList.SelectedItem = allGood;
                            if (allGood.SN == 1)
                            {
                                selectCompView.SNStackPanel.Visibility = Visibility.Visible;
                                //List<Model.DBTable.SerialNumber> serialNumbers = db.SerialNumbers.Where(arg => arg.IdGood == allGood.Id).ToList();
                                List<Model.DBTable.SerialNumber> serialNumbers = (from s in db.SerialNumbers
                                                                                  where s.IdGood == allGood.Id
                                                                                  where s.IdTransaction == 0 && s.IdComputer == 0
                                                                                  select s).ToList();
                                if (serialNumbers.Count == 0)
                                    return;
                                foreach (Model.DBTable.SerialNumber serialNumber in serialNumbers)
                                    selectCompView.SNCompComboBox.Items.Add(serialNumber.SN);
                            }
                        }
                        else
                            View.MessageBox.Show("Введенно не коректний штрих-код.", "", MessageBoxButton.OK, "Окей", null, null);
                    }));
            }
        }
        public RelayCommand FocusOnTextBoxCommand
        {
            get
            {
                return focusOnTextBoxCommand ??
                    (focusOnTextBoxCommand = new RelayCommand((parametr) =>
                    {
                        View.ConfiguratorView.SelectCompView selectCompView = parametr as View.ConfiguratorView.SelectCompView;
                        if (selectCompView.SNCompComboBox.IsKeyboardFocusWithin == true)
                        {
                            return;
                        }
                        ScanningBarCode = null;
                        selectCompView.BarCodeTextBox.Visibility = Visibility.Visible;
                        selectCompView.BarCodeTextBox.Focus();
                    }));
            }
        }
        public RelayCommand AcceptSelectCompViewCommand
        {
            get
            {
                return acceptSelectCompViewCommand ??
                    (acceptSelectCompViewCommand = new RelayCommand((parametr) =>
                    { 
                        View.ConfiguratorView.SelectCompView selectCompView = parametr as View.ConfiguratorView.SelectCompView;
                        if (selectCompView.selectedCompList.SelectedItem != null)
                        {
                            Model.AllGood allGood = selectCompView.selectedCompList.SelectedItem as Model.AllGood;
                            if(allGood.SN == 1)
                            {
                                foreach(string sn in selectCompView.SNCompComboBox.Items)
                                {
                                    if (selectCompView.SNCompComboBox.Text == sn)
                                    {
                                        selectCompView.DialogResult = true;
                                        return;
                                    }
                                }
                                View.MessageBox.Show("Введено не коректний серійний номер.", "", MessageBoxButton.OK, "Окей", null, null);
                            }
                            else
                                selectCompView.DialogResult = true;
                        }
                        else
                            View.MessageBox.Show("Виберіть комплектуючу із списку або проскануйте штрих-код.", "", MessageBoxButton.OK, "Окей", null, null);
                    }));
            }
        }
        public RelayCommand AddNewComponentCommand
        {
            get
            {
                return addNewComponentCommand ??
                    (addNewComponentCommand = new RelayCommand((parametr) =>
                    {
                        View.Pages.ConfiguratorPage configuratorPage = parametr as View.Pages.ConfiguratorPage;
                        ListBoxItem listBoxItem = new ListBoxItem();
                        for (int i = 0; i < configuratorPage.componentsList.Items.Count; i++)
                        {
                            listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(i));
                            if (listBoxItem.IsMouseOver == true)
                                break;
                        }
                        if (listBoxItem != null)
                        {
                            Model.AllComponent allComponent = listBoxItem.DataContext as Model.AllComponent;
                            int idGoodType = db.TypeGoods.Where(arg => arg.GoodType == allComponent.CompType).Select(arg => arg.Id).FirstOrDefault();
                            AllGoods = (from g in db.Goods
                                        join t in db.TypeGoods
                                        on g.GoodType equals t.Id
                                        where g.GoodType == idGoodType
                                        where g.Deleted == 0 && g.Quantity > 0
                                        select new Model.AllGood
                                        {
                                            Id = g.Id,
                                            Kode = g.Kode,
                                            Name = g.Name,
                                            Price = g.Price,
                                            SalePrice = g.SalePrice,
                                            Quantity = g.Quantity,
                                            Guarante = g.Guarante,
                                            GoodType = t.GoodType,
                                            Barcode = g.Barcode,
                                            Description = g.Description,
                                            Used = g.Used,
                                            Deleted = g.Deleted,
                                            SN = g.SN
                                        }).ToList();
                            //List <Model.DBTable.Good> goods = db.Goods.Where(arg => arg.GoodType == idGoodType).ToList();
                            if (AllGoods.Count > 0)
                            {
                                View.ConfiguratorView.SelectCompView selectCompView = new View.ConfiguratorView.SelectCompView();
                                selectCompView.DataContext = this;
                                selectCompView.BarCodeTextBox.Focus();
                                CompSN = null;
                                ScanningBarCode = null;
                                if (selectCompView.ShowDialog() == true)
                                {
                                    Ellipse ellipse = listBoxItem.FindChild<Ellipse>(string.Empty);
                                    ellipse.Visibility = Visibility.Collapsed;
                                    Rectangle rectangle = listBoxItem.FindChild<Rectangle>("UnderlineRectangle");
                                    rectangle.Fill = (Brush)brushConverter.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyUnderLineListBrush")));

                                    Model.AllGood allGood = selectCompView.selectedCompList.SelectedItem as Model.AllGood;

                                    /*Button button = listBoxItem.FindChild<Button>(string.Empty);
                                    button.Visibility = Visibility.Collapsed;*/
                                    /*TextBlock textBlock = listBoxItem.FindChild<TextBlock>("NameText");
                                    textBlock.Visibility = Visibility.Visible;*/

                                    allComponent.Name = allGood.Name;
                                    allComponent.Price = allGood.SalePrice;
                                    if (allGood.SN == 1)
                                        allComponent.SN = CompSN;
                                    else
                                        allComponent.SN = null;
                                    TotalComponentPrice += allGood.SalePrice;
                                    allComponent.IdGood = allGood.Id;
                                    /*Button delButton = listBoxItem.FindChild<Button>("DelCompButton");
                                    delButton.Visibility = Visibility.Visible;*/

                                    if(allComponent.CompType == "HDD" || allComponent.CompType == "SSD")
                                    {
                                        foreach(Model.AllComponent HDDorSSD in AllComponents.Where(arg => arg.CompType == "HDD" || arg.CompType == "SSD").ToList())
                                        {
                                            ListBoxItem HDDorSSDListBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(HDDorSSD.Id));
                                            //listBoxItem.BorderThickness = new Thickness(1);
                                            //listBoxItem.BorderBrush = Brushes.Red;
                                            Rectangle HDDorSSDRectangle = HDDorSSDListBoxItem.FindChild<Rectangle>("UnderlineRectangle");
                                            HDDorSSDRectangle.Fill = (Brush)brushConverter.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyLightGrayBrush")));
                                            Ellipse HDDorSSDEllipse = HDDorSSDListBoxItem.FindChild<Ellipse>(string.Empty);
                                            HDDorSSDEllipse.Visibility = Visibility.Collapsed;
                                        }
                                    }
                                    Model.DBTable.ConfiguratorComponent configuratorComponent = new ConfiguratorComponent();
                                    configuratorComponent.IdGood = allComponent.IdGood;
                                    configuratorComponent.SN = allComponent.SN;
                                    db.ConfiguratorComponents.Add(configuratorComponent);
                                    db.SaveChanges();
                                }
                            }
                            else
                                View.MessageBox.Show("Товару із даної категорії немає в БД.", "", MessageBoxButton.OK, "Окей", null, null);
                        }
                    }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
