﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using ToastNotifications;
using ToastNotifications.Position;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using System.Data.SQLite;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using System.Windows.Shapes;
using DevExpress.Mvvm.Native;
using IdealStore.Model;
using System.Windows.Documents;
using Microsoft.Xaml.Behaviors;
using ToastNotifications.Utilities;
using IdealStore.Model.DBTable;
using Xceed.Wpf.Toolkit.Converters;

namespace IdealStore.ViewModel
{
    public class PCViewModel : INotifyPropertyChanged
    {
        ApplicationContext db;
        RelayCommand pcSearchCommand;
        RelayCommand salePCCommand;
        RelayCommand editPCCommand;
        RelayCommand removeComponentCommand;
        RelayCommand addNewComponentCommand;
        RelayCommand acceptSelectCompViewCommand;
        RelayCommand changeSelectedItemCommand;
        RelayCommand addNewComponentTypeCommand;
        RelayCommand compTypeComboBoxDropDownClosedCommand;
        RelayCommand deletePCCommand;
        RelayCommand filterComboBoxDropDownClosedCommand;
        //RelayCommand changeEaringsSumCommand;
        BrushConverter brushConverter = new BrushConverter();

        string compSN;
        int totalComponentPrice;
        int totalPCPrice;
        int earningsSum;
        string filterParam;
        public int EarningsSum
        {
            get { return earningsSum; }
            set
            {
                earningsSum = value;
                TotalPCPrice = TotalComponentPrice + value; 
                OnPropertyChanged("EarningsSum");
            }
        }
        public int TotalPCPrice
        {
            get { return totalPCPrice; }
            set
            {
                totalPCPrice = value;
                OnPropertyChanged("TotalPCPrice");
            }
        }
        public int TotalComponentPrice
        {
            get { return totalComponentPrice; }
            set
            {
                totalComponentPrice = value;
                OnPropertyChanged("TotalComponentPrice");
            }
        }
        public string CompSN
        {
            get { return compSN; }
            set
            {
                compSN = value;
                OnPropertyChanged("CompSN");
            }
        }


        int[] idMandatoryComponents = new int[] { 10, 11, 17, 18 }; //ід обов'язкових комплектуючих 
        int[] idRepeatingСomponents = { 10, 27, 28 };
        int[] idType = { 10, 11, 12, 14, 15, 16, 17, 18, 27, 28 }; //ід типу комплектуючих


        ObservableCollection<Model.ReadyComputer> readyComputers;
        IEnumerable<Model.DBTable.Component> components;
        IEnumerable<Model.DBTable.Computer> computers;
        ObservableCollection<Model.AllComponent> allComponents;
        List<Model.AllGood> allGoods;

        public List<Model.AllGood> AllGoods
        {
            get { return allGoods; }
            set
            {
                allGoods = value;
                OnPropertyChanged("AllGoods");
            }
        }
        public IEnumerable<Model.DBTable.Computer> Computers
        {
            get { return computers; }
            set
            {
                computers = value;
                OnPropertyChanged("Computers");
            }
        }
        public IEnumerable<Model.DBTable.Component> Components
        {
            get { return components; }
            set
            {
                components = value;
                OnPropertyChanged("Componets");
            }
        }
        public ObservableCollection<Model.ReadyComputer> ReadyComputers
        {
            get { return readyComputers; }
            set
            {
                readyComputers = value;
                OnPropertyChanged("ReadyComputers");
            }
        }
        public ObservableCollection<Model.AllComponent> AllComponents
        {
            get { return allComponents; }
            set
            {
                allComponents = value;
                OnPropertyChanged("AllComponents");
            }
        }

        void ShowPCFunction()
        {
            db.Components.Load();
            ReadyComputers = new ObservableCollection<ReadyComputer>();
            Computers = db.Computers.Where(arg => arg.Sale == 0).ToList();
            foreach (Model.DBTable.Computer computer in Computers)
            {
                Model.ReadyComputer readyComputer = new Model.ReadyComputer();
                readyComputer.IdComputer = computer.Id;
                readyComputer.NameComponents = new ObservableCollection<string>();
                readyComputer.PCName = computer.PCName;
                readyComputer.Price = computer.TotalPcPrice;
                if (computer.Used == 1)
                    readyComputer.Used = "Б/в";
                else if (computer.Used == 0)
                    readyComputer.Used = "Новий";

                foreach (int idComponent in db.Components.Where(arg => arg.IdComputer == computer.Id).Select(arg => arg.IdGood).ToList())
                {
                    string name = db.Goods.Where(arg => arg.Id == idComponent).Select(arg => arg.Name).FirstOrDefault();
                    readyComputer.NameComponents.Add(name);
                }
                ReadyComputers.Add(readyComputer);
            }
            ReadyComputers = ReadyComputers;
            if (filterParam == "Нові")
                ReadyComputers = ReadyComputers.Where(arg => arg.Used == "Новий").ToObservableCollection();
            if (filterParam == "Б/в")
                ReadyComputers = ReadyComputers.Where(arg => arg.Used == "Б/в").ToObservableCollection();

        }

        public PCViewModel()
        {
            db = new ApplicationContext();
            ShowPCFunction();
        }

        public RelayCommand FilterComboBoxDropDownClosedCommand
        {
            get
            {
                return filterComboBoxDropDownClosedCommand ??
                    (filterComboBoxDropDownClosedCommand = new RelayCommand((parametr) =>
                    {
                        ComboBox comboBox = parametr as ComboBox;
                        if (comboBox == null)
                            return;
                        filterParam = comboBox.Text;
                        ShowPCFunction();
                        /*switch(comboBox.SelectedIndex)
                        {
                            case 0:
                                return;
                            case 1:
                                ReadyComputers = ReadyComputers.Where(arg => arg.Used == "Новий").ToObservableCollection();
                                return;
                            case 2:
                                ReadyComputers = ReadyComputers.Where(arg => arg.Used == "Б/в").ToObservableCollection();
                                return;
                        }*/
                    }));
            }
        }
        public RelayCommand DeletePCCommand
        {
            get
            {
                return deletePCCommand ??
                    (deletePCCommand = new RelayCommand((parametr) =>
                    {
                        Model.ReadyComputer readyComputer = parametr as Model.ReadyComputer;
                        if (readyComputer == null)
                            return;
                        MessageBoxResult messageBoxResult = View.MessageBox.Show("Ви бажаєте видалити " + readyComputer.PCName + "?", "", MessageBoxButton.YesNo, "Так", "Ні", null);
                        if (messageBoxResult == MessageBoxResult.Yes)
                        {
                            Model.DBTable.Computer computer = db.Computers.Where(arg => arg.Id == readyComputer.IdComputer).FirstOrDefault();
                            db.Entry(computer).State = EntityState.Deleted;
                            foreach (Model.DBTable.Component component in db.Components.Where(arg => arg.IdComputer == computer.Id).ToList())
                            {
                                db.Entry(component).State = EntityState.Deleted;
                                Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == component.IdGood).FirstOrDefault();
                                good.Quantity += 1;
                                good.Deleted = 0;
                                db.Entry(good).State = EntityState.Modified;
                                if (component.IdSN > 0)
                                {
                                    Model.DBTable.SerialNumber serialNumber = db.SerialNumbers.Where(arg => arg.Id == component.IdSN).FirstOrDefault();
                                    serialNumber.IdComputer = 0;
                                    db.Entry(serialNumber).State = EntityState.Modified;
                                }
                            }
                            db.SaveChanges();
                            ShowPCFunction();
                        }
                    }));
            }
        }
        public RelayCommand CompTypeComboBoxDropDownClosedCommand
        {
            get
            {
                return compTypeComboBoxDropDownClosedCommand ??
                    (compTypeComboBoxDropDownClosedCommand = new RelayCommand((parametr) =>
                    {
                        View.PCView.EditPCView editPCView = parametr as View.PCView.EditPCView;
                        ListBoxItem listBoxItem = (ListBoxItem)(editPCView.componentsList.ItemContainerGenerator.ContainerFromIndex(editPCView.componentsList.Items.Count - 1));
                        ComboBox comboBox = listBoxItem.FindChild<ComboBox>("CompTypeComboBox");
                        Button button = listBoxItem.FindChild<Button>("AddNewCompTypeButton");
                        if (comboBox.Text.Length == 0)
                        {
                            MessageBoxResult messageBoxResult = View.MessageBox.Show("Не вибраний тип комплектуючої. Ви бажаєте відмінити вибір?", "", MessageBoxButton.YesNo, "Так", "Ні", null);
                            if (messageBoxResult == MessageBoxResult.Yes)
                            {
                                comboBox.Visibility = Visibility.Collapsed;
                                button.IsEnabled = true;
                            }
                            if (messageBoxResult == MessageBoxResult.No || messageBoxResult == MessageBoxResult.None)
                            {
                                comboBox.IsDropDownOpen = true;
                            }
                            return;
                        }
                        else
                        {
                            comboBox.Visibility = Visibility.Collapsed;
                            AllComponents.LastOrDefault().CompType = comboBox.Text;
                            AllComponents.LastOrDefault().IdComputer = AllComponents.FirstOrDefault().IdComputer;
                            //Model.DBTable.Component component = new Model.DBTable.Component();
                            AllComponents.Add(new AllComponent());
                        }
                    }));
            }
        }
        public RelayCommand AddNewComponentTypeCommand
        {
            get
            {
                return addNewComponentTypeCommand ??
                    (addNewComponentTypeCommand = new RelayCommand((parametr) =>
                    {
                        View.PCView.EditPCView editPCView = parametr as View.PCView.EditPCView;
                        ListBoxItem listBoxItem = (ListBoxItem)(editPCView.componentsList.ItemContainerGenerator.ContainerFromIndex(editPCView.componentsList.Items.Count - 1));
                        ComboBox comboBox = listBoxItem.FindChild<ComboBox>("CompTypeComboBox");
                        comboBox.Visibility = Visibility.Visible;
                        comboBox.IsDropDownOpen = true;
                        comboBox.Items.Clear();
                        foreach (int id in idRepeatingСomponents)
                        {
                            string typeComp = db.TypeGoods.Where(arg => arg.Id == id).Select(arg => arg.GoodType).FirstOrDefault();
                            switch (typeComp)
                            {
                                case "Оперативна пам'ять":
                                    if (AllComponents.Where(arg => arg.CompType == typeComp).Count() < 4)
                                        comboBox.Items.Add(typeComp);
                                    break;
                                case "HDD":
                                case "SSD":
                                    if (AllComponents.Where(arg => arg.CompType == typeComp).Count() < 3)
                                        comboBox.Items.Add(typeComp);
                                    break;
                            }
                        }

                        Button button = listBoxItem.FindChild<Button>("AddNewCompTypeButton");
                        button.IsEnabled = false;
                    }));
            }
        }
        public RelayCommand ChangeSelectedItemCommand
        {
            get
            {
                return changeSelectedItemCommand ??
                    (changeSelectedItemCommand = new RelayCommand((parametr) =>
                    {
                        View.ConfiguratorView.SelectCompView selectCompView = parametr as View.ConfiguratorView.SelectCompView;
                        Model.AllGood allGood = selectCompView.selectedCompList.SelectedItem as Model.AllGood;
                        if (allGood == null)
                            return;
                        if (allGood.SN == 1)
                        {
                            CompSN = null;
                            selectCompView.SNStackPanel.Visibility = Visibility.Visible;
                            List<Model.DBTable.SerialNumber> serialNumbers = (from s in db.SerialNumbers
                                                                              where s.IdGood == allGood.Id
                                                                              where s.IdTransaction == 0 && s.IdComputer == 0
                                                                              select s).ToList();
                            foreach (string snRemove in AllComponents.Select(arg => arg.SN))
                                serialNumbers.Remove(serialNumbers.Where(arg => arg.SN == snRemove).FirstOrDefault());
                            selectCompView.SNCompComboBox.Items.Clear();
                            foreach (Model.DBTable.SerialNumber serialNumber in serialNumbers)
                            {
                                selectCompView.SNCompComboBox.Items.Add(serialNumber.SN);
                            }
                        }
                    }));
            }
        }
        public RelayCommand AcceptSelectCompViewCommand
        {
            get
            {
                return acceptSelectCompViewCommand ??
                    (acceptSelectCompViewCommand = new RelayCommand((parametr) =>
                    {
                        View.ConfiguratorView.SelectCompView selectCompView = parametr as View.ConfiguratorView.SelectCompView;
                        selectCompView.DialogResult = true;
                    }));
            }
        }
        public RelayCommand AddNewComponentCommand
        {
            get
            {
                return addNewComponentCommand ??
                    (addNewComponentCommand = new RelayCommand((parametr) =>
                    {
                        View.PCView.EditPCView editPCView = parametr as View.PCView.EditPCView;
                        ListBoxItem listBoxItem = new ListBoxItem();
                        for (int i = 0; i < editPCView.componentsList.Items.Count; i++)
                        {
                            listBoxItem = (ListBoxItem)(editPCView.componentsList.ItemContainerGenerator.ContainerFromIndex(i));
                            if (listBoxItem.IsMouseOver == true)
                                break;
                        }
                        if (listBoxItem != null)
                        {
                            Model.AllComponent allComponent = listBoxItem.DataContext as Model.AllComponent;
                            int idGoodType = db.TypeGoods.Where(arg => arg.GoodType == allComponent.CompType).Select(arg => arg.Id).FirstOrDefault();
                            AllGoods = (from g in db.Goods
                                        join t in db.TypeGoods
                                        on g.GoodType equals t.Id
                                        where g.GoodType == idGoodType
                                        where g.Deleted == 0 && g.Quantity > 0
                                        select new Model.AllGood
                                        {
                                            Id = g.Id,
                                            Kode = g.Kode,
                                            Name = g.Name,
                                            Price = g.Price,
                                            SalePrice = g.SalePrice,
                                            Quantity = g.Quantity,
                                            Guarante = g.Guarante,
                                            GoodType = t.GoodType,
                                            Barcode = g.Barcode,
                                            Description = g.Description,
                                            Used = g.Used,
                                            Deleted = g.Deleted,
                                            SN = g.SN
                                        }).ToList();
                            if (AllGoods.Count > 0)
                            {
                                View.ConfiguratorView.SelectCompView selectCompView = new View.ConfiguratorView.SelectCompView();
                                selectCompView.DataContext = this;
                                selectCompView.BarCodeTextBox.Focus();
                                CompSN = null;
                                if (selectCompView.ShowDialog() == true)
                                {
                                    /*Ellipse ellipse = listBoxItem.FindChild<Ellipse>(string.Empty);
                                    ellipse.Visibility = Visibility.Collapsed;
                                    Rectangle rectangle = listBoxItem.FindChild<Rectangle>("UnderlineRectangle");
                                    rectangle.Fill = (Brush)brushConverter.ConvertFrom(Convert.ToString(Application.Current.FindResource("MyUnderLineListBrush")));
                                    */

                                    Model.AllGood allGood = selectCompView.selectedCompList.SelectedItem as Model.AllGood;
                                    allComponent.Name = allGood.Name;
                                    allComponent.Price = allGood.SalePrice;
                                    if (allGood.SN == 1)
                                        allComponent.SN = CompSN;
                                    else
                                        allComponent.SN = null;
                                    allComponent.IdGood = allGood.Id;
                                    TotalPCPrice += allComponent.Price;
                                    TotalComponentPrice += allComponent.Price;
                                    Model.DBTable.Component component = new Model.DBTable.Component();
                                    if (allComponent.Id > 0)
                                    {
                                        component = db.Components.Where(arg => arg.Id == allComponent.Id).FirstOrDefault();
                                        component.IdGood = allComponent.IdGood;
                                        component.Price = allComponent.Price;
                                        if (allComponent.SN != null && allComponent.SN.Length > 0)
                                        {
                                            Model.DBTable.SerialNumber serialNumber = db.SerialNumbers.Where(arg => arg.SN == allComponent.SN).FirstOrDefault();
                                            serialNumber.IdComputer = allComponent.IdComputer;
                                            db.Entry(serialNumber).State = EntityState.Modified;
                                            component.IdSN = serialNumber.Id;
                                        }
                                        db.Entry(component).State = EntityState.Modified;
                                    }
                                    else
                                    {
                                        component.IdComputer = allComponent.IdComputer;
                                        component.IdGood = allComponent.IdGood;
                                        component.IdType = db.TypeGoods.Where(arg => arg.GoodType == allComponent.CompType).Select(arg => arg.Id).FirstOrDefault();
                                        component.Price = allComponent.Price;
                                        if (allComponent.SN.Length > 0)
                                        {
                                            Model.DBTable.SerialNumber serialNumber = db.SerialNumbers.Where(arg => arg.SN == allComponent.SN).FirstOrDefault();
                                            serialNumber.IdComputer = allComponent.IdComputer;
                                            db.Entry(serialNumber).State = EntityState.Modified;
                                            component.IdSN = serialNumber.Id;
                                        }
                                        db.Components.Add(component);
                                        //Перевірити додвання ід в нові компоненти (АллКомпонент)
                                    }
                                    Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == allComponent.IdGood).FirstOrDefault();
                                    good.Quantity -= 1;
                                    db.SaveChanges();
                                    allComponent.Id = db.Components.OrderByDescending(arg => arg.Id).Select(arg => arg.Id).FirstOrDefault();
                                }
                            }
                            else
                                View.MessageBox.Show("Товару із даної категорії немає в БД.", "", MessageBoxButton.OK, "Окей", null, null);
                        }                        
                    }));
            }
        }
        public RelayCommand RemoveComponentCommand
        {
            get
            {
                return removeComponentCommand ??
                    (removeComponentCommand = new RelayCommand((parametr) =>
                    {
                        ListBox listBox = parametr as ListBox;
                        ListBoxItem listBoxItem = new ListBoxItem();
                        for(int i = 0; i < listBox.Items.Count; i++)
                        {
                            listBoxItem = (ListBoxItem)listBox.ItemContainerGenerator.ContainerFromIndex(i);
                            if (listBoxItem.IsMouseOver == true)
                                break;
                        //ListBoxItem listBoxItem = (ListBoxItem)(configuratorPage.componentsList.ItemContainerGenerator.ContainerFromIndex(configuratorPage.componentsList.Items.Count - 1));
                        }
                        Model.AllComponent allComponent = listBoxItem.DataContext as Model.AllComponent;
                        // 27 28 HDD or SSD
                        Good good = db.Goods.Where(arg => arg.Id == allComponent.IdGood).FirstOrDefault();
                        good.Quantity += 1;
                        good.Deleted = 0;
                        db.Entry(good).State = EntityState.Modified;
                        if (good.SN == 1)
                        {
                            SerialNumber serialNumber = db.SerialNumbers.Where(arg => arg.SN == allComponent.SN).FirstOrDefault();
                            serialNumber.IdComputer = 0;
                            db.Entry(serialNumber).State = EntityState.Modified;
                        }
                        Model.DBTable.Component component = db.Components.Where(arg => arg.Id == allComponent.Id).FirstOrDefault();
                        TotalPCPrice -= component.Price;
                        TotalComponentPrice -= component.Price;
                        if (component.IdType == 27 || component.IdType == 28)
                        {
                            if(AllComponents.Where(arg => arg.CompType == "HDD" || arg.CompType == "SSD").Count() == 1)
                            {
                                component.IdGood = 0;
                                component.IdSN = 0;
                                component.Price = 0;
                                db.Entry(component).State = EntityState.Modified;
                                allComponent.IdGood = 0;
                                allComponent.SN = null;
                                allComponent.Price = 0;
                                allComponent.Name = null;
                            }
                            else
                            {
                                db.Components.Remove(component);
                                AllComponents.Remove(allComponent);
                            }
                            db.SaveChanges();
                            return;
                        }

                        if (idMandatoryComponents.Where(arg => arg == db.TypeGoods.Where(arg1 => arg1.GoodType == allComponent.CompType).Select(arg1 => arg1.Id).FirstOrDefault()).FirstOrDefault() > 0 
                        && AllComponents.Where(arg => arg.CompType == allComponent.CompType).Count() == 1)
                        {
                            component.IdGood = 0;
                            component.IdSN = 0;
                            component.Price = 0;
                            db.Entry(component).State = EntityState.Modified;
                            allComponent.IdGood = 0;
                            allComponent.SN = null;
                            allComponent.Price = 0;
                            allComponent.Name = null;
                        }
                        else
                        {
                            db.Components.Remove(component);
                            AllComponents.Remove(allComponent);
                        }
                        db.SaveChanges();
                        
                    }));
            }
        }
        public RelayCommand EditPCCommand
        {
            get
            {
                return editPCCommand ??
                    (editPCCommand = new RelayCommand((parametr) =>
                    {
                        Model.ReadyComputer readyComputer = parametr as Model.ReadyComputer;
                        if (readyComputer == null)
                            return;
                        AllComponents = new ObservableCollection<AllComponent>();
                        foreach (Model.DBTable.Component component in db.Components.Where(arg => arg.IdComputer == readyComputer.IdComputer).ToList())
                        {
                            Model.AllComponent allComponent = new Model.AllComponent();
                            allComponent.CompType = db.TypeGoods.Where(arg => arg.Id == component.IdType).Select(arg => arg.GoodType).FirstOrDefault();
                            allComponent.Id = component.Id;
                            allComponent.IdComputer = component.IdComputer;
                            Model.DBTable.Good good = db.Goods.Where(arg => arg.Id == component.IdGood).FirstOrDefault();
                            if (good == null)
                            {
                                //allComponent.CompType = db.TypeGoods.Where(arg => arg.Id == good.GoodType).Select(arg => arg.GoodType).FirstOrDefault();
                                AllComponents.Add(allComponent);
                                continue;
                            }
                            allComponent.IdGood = component.IdGood;
                            allComponent.Name = good.Name;
                            allComponent.Price = component.Price;
                            allComponent.SN = db.SerialNumbers.Where(arg => arg.Id == component.IdSN).Select(arg => arg.SN).FirstOrDefault();
                            AllComponents.Add(allComponent);
                            totalComponentPrice += component.Price;
                        }
                        AllComponents.Add(new AllComponent());
                        View.PCView.EditPCView editPCView = new View.PCView.EditPCView();
                        editPCView.DataContext = this;
                        editPCView.NamePCTextBlock.Text = readyComputer.PCName;
                        TotalPCPrice = readyComputer.Price;
                        TotalComponentPrice = db.Components.Where(arg => arg.IdComputer == readyComputer.IdComputer).Sum(arg => arg.Price);
                        EarningsSum = TotalPCPrice - TotalComponentPrice;
                        editPCView.ShowDialog();
                        Model.DBTable.Computer computer = db.Computers.Where(arg => arg.Id == readyComputer.IdComputer).FirstOrDefault();
                        computer.TotalPcPrice = TotalPCPrice;
                        db.Entry(computer).State = EntityState.Modified;
                        db.SaveChanges();
                        //if(db.Components.Where(arg => arg.IdComputer == readyComputer.IdComputer).Where(arg => arg.IdGood == 0).Count() > 0)
                        //{

                        //}
                        ShowPCFunction();
                    }));
            }
        }
        public RelayCommand SalePCCommand
        {
            get
            {
                return salePCCommand ??
                    (salePCCommand = new RelayCommand((parametr) =>
                    {
                        Model.ReadyComputer readyComputer = parametr as Model.ReadyComputer;
                        if (readyComputer == null)
                            return;
                        foreach (string nameComponent in readyComputer.NameComponents)
                        {
                            if (nameComponent == null || nameComponent == string.Empty)
                            {
                                View.MessageBox.Show("Продаж ПК неможливий. Додайте всі обов'язкові компоненти.", "", MessageBoxButton.OK, "Окей", null, null);
                                return;
                            }
                        }
                        MessageBoxResult messageBoxResult = View.MessageBox.Show("Продати " + readyComputer.PCName + " за " + readyComputer.Price + " грн.", "", MessageBoxButton.YesNo, "Так, продати", "Ні, скасувати", "");
                        if (messageBoxResult == MessageBoxResult.No || messageBoxResult == MessageBoxResult.None)
                            return;
                        Model.DBTable.Computer computer = db.Computers.Where(arg => arg.Id == readyComputer.IdComputer).FirstOrDefault();
                        computer.Sale = 1;
                        db.Entry(computer).State = EntityState.Modified;
                        db.SaveChanges();
                        ShowPCFunction();
                    }));
            }
        }
        public RelayCommand PCSearchCommand
        {
            get
            {
                return pcSearchCommand ??
                    (pcSearchCommand = new RelayCommand((parametr) =>
                    {
                        string searchText = parametr as string;
                        if (searchText == null || searchText == string.Empty)
                        {
                            ShowPCFunction();
                            return;
                        }
                        bool remove = true;
                        for (int i = 0; i < ReadyComputers.Count(); i++)
                        {
                            //Перевірка, чи вміщає в собі ім'я компоненти текст пошуку .... Contains.
                            foreach(string nameComp in ReadyComputers[i].NameComponents)
                            {
                                if(nameComp != null && nameComp.ToLower().Contains(searchText.ToLower()))
                                {
                                    remove = false;
                                    break;
                                }
                                remove = true;
                            }
                            if (ReadyComputers[i].PCName.ToLower().Contains(searchText.ToLower()))
                                remove = false;
                            if (remove)
                                ReadyComputers.Remove(ReadyComputers[i]);
                        }    
                    }));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
