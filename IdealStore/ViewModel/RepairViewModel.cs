﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Data;
using System.Windows.Controls;
using AutoMapper;
using AutoMapper.Configuration;
using AutoMapper.Execution;
using AutoMapper.Features;
using AutoMapper.Internal;
using AutoMapper.Mappers;
using AutoMapper.QueryableExtensions;
using System.Windows.Media;

namespace IdealStore.ViewModel
{
    public class RepairViewModel : DependencyObject, INotifyPropertyChanged
    {
        ApplicationContext db;
        RelayCommand repairDeliveryCommand;
        RelayCommand repairFilterCommand;
        RelayCommand repairAddCommand;
        RelayCommand repairEditCommand;
        RelayCommand repairDeleteCommand;
        RelayCommand repairFindCommand;
        RelayCommand acceptAddEditCommand;
        IEnumerable<Model.DBTable.Repair> repairs;
        IEnumerable<Model.RepairResultFilter> repairsResultFilter;
        List<Model.DBTable.TypeTechnicsRepairs> typeTechnicsRepairs;

        BrushConverter brushConverter;
        Model.RepairResultFilter repairResultFilter;

        public Model.RepairResultFilter RepairResultFilter
        {
            get { return repairResultFilter; }
            set
            {
                repairResultFilter = value;
                OnPropertyChanged("RepairResultFilter");
            }
        }

        string repairFilterParam;

        public string RepairFilterParam
        {
            get { return repairFilterParam; }
            set
            {
                repairFilterParam = value;
                OnPropertyChanged("RepairFilterParam");
            }
        }
        public List<Model.DBTable.TypeTechnicsRepairs> TypeTechnicsRepairs
        {
            get { return typeTechnicsRepairs; }
            set
            {
                typeTechnicsRepairs = value;
                OnPropertyChanged("TypeTechnicsRepairs");
            }
        }
        public IEnumerable<Model.RepairResultFilter> RepairsResultFilter
        {
            get { return repairsResultFilter; }
            set
            {
                repairsResultFilter = value;
                OnPropertyChanged("RepairsResultFilter");
            }
        }
        public IEnumerable<Model.DBTable.Repair> Repairs
        {
            get { return repairs; }
            set
            {
                repairs = value;
                OnPropertyChanged("Repairs");
            }
        }
        public void RepResFilt()
        {
            db = new ApplicationContext();
            RepairsResultFilter = (from r in db.Repairs
                                   join t in db.TypeTechnicsRepairs
                                   on r.TypeTechnics equals t.Id 
                                   where r.Deleted == 0
                                   orderby r.Id descending
                                   select new Model.RepairResultFilter
                                   {
                                       Id = r.Id,
                                       Date = r.Date,
                                       DescriptionProblem = r.DescriptionProblem,
                                       NameOwner = r.NameOwner,
                                       NumberOwner = r.NumberOwner,
                                       Price = r.Price,
                                       PriceOwner = r.PriceOwner,
                                       DateSecond = r.DateSecond,
                                       Comment = r.Comment,
                                       Deleted = r.Deleted,
                                       ExternalRep = r.ExternalRep,
                                       Name = r.Name,
                                       TypeTechnics = t.Type,
                                       SN = r.SN
                                   }).ToList();
            if (repairFilterParam == "Невидані ремонти")
                RepairsResultFilter = RepairsResultFilter.Where(arg => arg.DateSecond == null || arg.DateSecond == string.Empty).ToList();
            if (repairFilterParam == "Видані ремонти")
                RepairsResultFilter = RepairsResultFilter.Where(arg => arg.Date != null && arg.DateSecond.Length > 0).ToList();
            if (repairSearchText != null)
            {
                string searchText = repairSearchText.ToLower();

                RepairsResultFilter = RepairsResultFilter.Where(arg => arg.Date.ToLower().Contains(searchText)
                || arg.DescriptionProblem.ToLower().Contains(searchText) || arg.NameOwner.ToLower().Contains(searchText)
                || arg.NumberOwner.Contains(searchText) || arg.DateSecond.ToLower().Contains(searchText)
                || arg.Comment.ToLower().Contains(searchText) || arg.SN.ToLower().Contains(searchText)
                || arg.Name.ToLower().Contains(searchText) || arg.TypeTechnics.ToLower().Contains(searchText));
            }

        }
        /*public void BrandCheck(dynamic externalRepair, dynamic RepairResultFilter)
        {
            bool check = false;

            char[] charsCheck = RepairResultFilter.Brand.ToCharArray();
            for (int i = 0; charsCheck.Length > i; i++)
            {
                charsCheck[i] = char.ToLower(charsCheck[i]);
            }

            string brCheck = new string(charsCheck);

            foreach (var dbBrandCheck in Brands.Select(b => b.BrandName))
            {
                char[] dbCharsCheck = dbBrandCheck.ToCharArray();
                for (int i = 0; dbCharsCheck.Length > i; i++)
                {
                    dbCharsCheck[i] = char.ToLower(dbCharsCheck[i]);
                }
                string dbBrCheck = new string(dbCharsCheck);
                if (dbBrCheck == brCheck)
                {
                    check = true;
                }
                if (check == true)
                {
                    externalRepair.BrandId = (from b in db.Brands
                                              where b.BrandName == dbBrandCheck
                                              select b.Id).FirstOrDefault();
                    break;
                }
            }
            if (check == false)
            {
                Model.DBTable.Brand brand = new Model.DBTable.Brand();
                brand.BrandName = RepairResultFilter.Brand;
                db.Brands.Add(brand);
                externalRepair.BrandId = ((from b in db.Brands
                                           orderby b.Id descending
                                           select b.Id).FirstOrDefault()) + 1;
            }
        }*/
        public RepairViewModel()
        {
            RepResFilt();
            brushConverter = new BrushConverter();
        }


        View.RepairView.AddRepairView AddRepairViewShowFunction()
        {
            View.RepairView.AddRepairView addRepairView = new View.RepairView.AddRepairView();
            addRepairView.DataContext = RepairResultFilter;
            TypeTechnicsRepairs = db.TypeTechnicsRepairs.ToList();
            addRepairView.TypeTechnicsText.ItemsSource = TypeTechnicsRepairs.Select(arg => arg.Type);
            addRepairView.ButtonStackPanel.DataContext = this;
            return addRepairView;
        }

        // змінні для пошуку
        public static readonly DependencyProperty searchProperty = DependencyProperty.Register("repairSearchText", typeof(string), typeof(RepairViewModel));
        public string repairSearchText
        {
            get { return (string)GetValue(searchProperty); }
            set { SetValue(searchProperty, value); }
        }

        public RelayCommand AcceptAddEditCommand
        {
            get
            {
                return acceptAddEditCommand ??
                    (acceptAddEditCommand = new RelayCommand((parametr) =>
                    {
                        View.RepairView.AddRepairView addRepairView = parametr as View.RepairView.AddRepairView;
                        bool addError = false;
                        if(RepairResultFilter.NameOwner == null || RepairResultFilter.NameOwner == string.Empty)
                        {
                            addRepairView.NameOwnText.BorderBrush = Brushes.Red;
                            addError = true;
                        }
                        if (RepairResultFilter.NumberOwner == null || RepairResultFilter.NumberOwner == string.Empty)
                        {
                            addRepairView.NumOwnText.BorderBrush = Brushes.Red;
                            addError = true;
                        }
                        if (RepairResultFilter.DescriptionProblem == null || RepairResultFilter.DescriptionProblem == string.Empty)
                        {
                            addRepairView.DescProbText.BorderBrush = Brushes.Red;
                            addError = true;
                        }
                        if (RepairResultFilter.TypeTechnics == null || RepairResultFilter.TypeTechnics == string.Empty)
                        {
                            addRepairView.TypeTechnicsText.BorderBrush = Brushes.Red;
                            addError = true;
                        }

                        if (addError == false)
                            addRepairView.DialogResult = true;
                        else
                            View.MessageBox.Show("Заповніть всі обов'язкові поля.", "", MessageBoxButton.OK, "Окей", null, null);
                    }));
            }
        }
        public RelayCommand RepairFindCommand
        {
            get
            {
                return repairFindCommand ??
                    (repairFindCommand = new RelayCommand((o)=>
                    {
                        RepResFilt();
                    }));
            }
        }
        public RelayCommand RepairFilterCommand
        {
            get
            {
                return repairFilterCommand ??
                    (repairFilterCommand = new RelayCommand((o) =>
                    {
                        View.RepairView.RepairTabFilterView repairTabFilterView = new View.RepairView.RepairTabFilterView();
                        //repairTabFilterView.
                        RadioButton checkedButton = repairTabFilterView.FilterRadioButtonStackPanel.Children.OfType<RadioButton>().FirstOrDefault(arg => arg.Content.ToString() == repairFilterParam);
                        if (checkedButton != null)
                            checkedButton.IsChecked = true;
                        else
                            repairTabFilterView.AllRepairRadioButton.IsChecked = true;
                        if (repairTabFilterView.ShowDialog() == true)
                        {
                            checkedButton = repairTabFilterView.FilterRadioButtonStackPanel.Children.OfType<RadioButton>()
                            .FirstOrDefault(arg => arg.IsChecked == true) as RadioButton;
                            repairFilterParam = checkedButton.Content.ToString(); 
                            RepResFilt();
                            /*if (repairTabFilterView.ExternalRep.IsChecked == true)
                            {
                                RepairsResultFilter = (from r in RepairsResultFilter
                                                       where r.ExternalRep == 1
                                                       select r);
                            }
                            else if (repairTabFilterView.InternalRep.IsChecked == true)
                            {
                                RepairsResultFilter = (from r in RepairsResultFilter
                                                       where r.ExternalRep == 0
                                                       select r);
                            }
                            else if (repairTabFilterView.DoneRep.IsChecked == true)
                            {
                                RepairsResultFilter = (from r in RepairsResultFilter
                                                       where r.Done == 1
                                                       select r);
                            }
                            RepairsResultFilter = RepairsResultFilter.ToList(); */
                        }
                    }
                    ));
            }
        }
        public RelayCommand RepairAddCommand
        {
            get
            {
                return repairAddCommand ??
                  (repairAddCommand = new RelayCommand((o) =>
                  {
                      RepairResultFilter = new Model.RepairResultFilter();
                      View.RepairView.AddRepairView addRepairView = new View.RepairView.AddRepairView();
                      addRepairView.Title = "Прийом техніки";
                      TypeTechnicsRepairs = db.TypeTechnicsRepairs.ToList();
                      addRepairView.DataContext = RepairResultFilter;
                      addRepairView.TypeTechnicsText.ItemsSource = TypeTechnicsRepairs.Select(arg => arg.Type);
                      addRepairView.ButtonStackPanel.DataContext = this;

                      if (addRepairView.ShowDialog() == true)
                      {
                          Model.DBTable.Repair repair = new Model.DBTable.Repair();

                          repair.DescriptionProblem = RepairResultFilter.DescriptionProblem;
                          repair.NameOwner = RepairResultFilter.NameOwner;
                          repair.NumberOwner = RepairResultFilter.NumberOwner != null ? RepairResultFilter.NumberOwner: string.Empty;
                          repair.Price = RepairResultFilter.Price;
                          repair.PriceOwner = RepairResultFilter.PriceOwner;
                          repair.Comment = RepairResultFilter.Comment != null ? RepairResultFilter.Comment: string.Empty;
                          repair.Date = DateTime.Now.ToString("dd/MM/yyyy");
                          repair.DateSecond = string.Empty;
                          repair.SN = RepairResultFilter.SN != null ? RepairResultFilter.SN : string.Empty;
                          repair.Name = RepairResultFilter.Name != null ? RepairResultFilter.Name : string.Empty;
                          repair.TypeTechnics = db.TypeTechnicsRepairs.Where(arg => arg.Type == RepairResultFilter.TypeTechnics).Select(arg => arg.Id).FirstOrDefault();

                          if (checked(addRepairView.ExtRepCheck.IsChecked) == true)
                          {
                              repair.ExternalRep = 1;
                          }
                          db.Repairs.Add(repair);
                          db.SaveChanges();
                          RepResFilt();
                      }
                  }));
            }
        }
        public RelayCommand RepairEditCommand
        {
            get
            {
                return repairEditCommand ??
                  (repairEditCommand = new RelayCommand((selectedItem) =>
                  {
                      if (selectedItem == null) return;
                      RepairResultFilter = new Model.RepairResultFilter();
                      RepairResultFilter = selectedItem as Model.RepairResultFilter;

                      if (RepairResultFilter.DateSecond == null || RepairResultFilter.DateSecond == string.Empty)
                      {

                          View.RepairView.AddRepairView addRepairView = new View.RepairView.AddRepairView();
                          addRepairView.Title = "Редагування ремонту";
                          addRepairView.DataContext = RepairResultFilter;
                          TypeTechnicsRepairs = db.TypeTechnicsRepairs.ToList();
                          addRepairView.TypeTechnicsText.ItemsSource = TypeTechnicsRepairs.Select(arg => arg.Type);
                          addRepairView.ButtonStackPanel.DataContext = this;
                          if (RepairResultFilter.ExternalRep == 1)
                          {
                              addRepairView.ExtRepCheck.IsChecked = true;
                          }

                          if (addRepairView.ShowDialog() == true)
                          {
                              Model.DBTable.Repair repair = new Model.DBTable.Repair();
                              repair = db.Repairs.Find(RepairResultFilter.Id);

                              if (repair != null)
                              {
                                  repair.DescriptionProblem = RepairResultFilter.DescriptionProblem != null ? RepairResultFilter.DescriptionProblem : string.Empty;
                                  repair.NameOwner = RepairResultFilter.NameOwner;
                                  repair.NumberOwner = RepairResultFilter.NumberOwner;
                                  repair.Price = RepairResultFilter.Price;
                                  repair.PriceOwner = RepairResultFilter.PriceOwner;
                                  repair.Comment = RepairResultFilter.Comment != null ? RepairResultFilter.Comment : string.Empty;
                                  repair.SN = RepairResultFilter.SN != null ? RepairResultFilter.SN : string.Empty;
                                  repair.TypeTechnics = db.TypeTechnicsRepairs.Where(arg => arg.Type == RepairResultFilter.TypeTechnics).Select(arg => arg.Id).FirstOrDefault();
                                  repair.Name = RepairResultFilter.Name != null ? RepairResultFilter.Name : string.Empty;
                                  if (checked(addRepairView.ExtRepCheck.IsChecked) == true)
                                  {
                                      repair.ExternalRep = 1;
                                  }
                                  else
                                  {
                                      repair.ExternalRep = 0;
                                  }
                                  
                                  db.Entry(repair).State = EntityState.Modified;
                                  db.SaveChanges();
                                  RepResFilt();
                              }
                          }
                      }
                      else
                      { View.MessageBox.Show("Ремонт уже виданий. Редагування не можливе.", "",
                          MessageBoxButton.OK,"Окей",null,null);
                      }
                  }));
            }
        }
        public RelayCommand RepairDeliveryCommand
        {
            get
            {
                return repairDeliveryCommand ??
                  (repairDeliveryCommand = new RelayCommand((selectedItem) =>
                  {
                      if (selectedItem == null) return;
                      // получаем выделенный объект
                      RepairResultFilter = selectedItem as Model.RepairResultFilter;
                      Model.DBTable.Repair repair = (from r in db.Repairs where r.Id == RepairResultFilter.Id select r).ToList().SingleOrDefault();
                      if (RepairResultFilter.DateSecond == null || RepairResultFilter.DateSecond == string.Empty)
                      {
                          repair.DateSecond = DateTime.Today.ToString("dd/MM/yyyy");
                          MessageBoxResult result = View.MessageBox.Show("До сплати " + repair.PriceOwner + " грн. Ви бажаєте сплатити повну суму?", "",
                              MessageBoxButton.YesNoCancel,"Так, видати з повною оплатою",
                          "Ні, сплати за діагностику", "Скасувати");
                          switch (result)
                          {
                              case MessageBoxResult.Yes:
                                  db.Entry(repair).State = EntityState.Modified;
                                  db.SaveChanges();
                                  break;
                              case MessageBoxResult.No:
                                  View.RepairView.AddRepairView addRepairView = new View.RepairView.AddRepairView();

                                  RepairResultFilter = RepairResultFilter;

                                  addRepairView.Title = "Змінна ціни";
                                  addRepairView = AddRepairViewShowFunction();

                                  if (repair.ExternalRep == 1)
                                  {
                                      addRepairView.ExtRepCheck.IsChecked = true;
                                  }
                                  addRepairView.NameOwnText.IsEnabled = false;
                                  addRepairView.NumOwnText.IsEnabled = false;
                                  addRepairView.DescProbText.IsEnabled = false;
                                  addRepairView.TypeTechnicsText.IsEnabled = false;
                                  addRepairView.ExtRepCheck.IsEnabled = false;
                                  addRepairView.NameText.IsEnabled = false;
                                  addRepairView.SNText.IsEnabled = false;
                                  addRepairView.ComentText.IsEnabled = false;

                                  addRepairView.NameOwnText.Opacity = 0.6;
                                  addRepairView.NumOwnText.Opacity = 0.6;
                                  addRepairView.DescProbText.Opacity = 0.6;
                                  addRepairView.TypeTechnicsText.Opacity = 0.6;
                                  addRepairView.ExtRepCheck.Opacity = 0.6;
                                  addRepairView.NameText.Opacity = 0.6;
                                  addRepairView.SNText.Opacity = 0.6;
                                  addRepairView.ComentText.Opacity = 0.6;
                                  addRepairView.AcceptAddRepButton.Content = "Видати";


                                  if (addRepairView.ShowDialog() == true)
                                  {
                                      if (repair != null)
                                      {
                                          repair.Price = RepairResultFilter.Price;
                                          repair.PriceOwner = RepairResultFilter.PriceOwner;
                                          db.Entry(repair).State = EntityState.Modified;
                                          db.SaveChanges();
                                      }
                                  }
                                  break;
                              case MessageBoxResult.Cancel:
                                  break;
                          }
                          RepResFilt();
                      }
                      else { View.MessageBox.Show("Ремонт уже виданий. Повторна видача не можлива.", "", MessageBoxButton.OK, "Окей", null, null); }
                  }));
            }
        }
        public RelayCommand RepairDeleteCommand
        {
            get
            {
                return repairDeleteCommand ??
                  (repairDeleteCommand = new RelayCommand((selectedItem) =>
                  {
                      if (selectedItem == null)
                          return;
                      Model.RepairResultFilter RepairResultFilter = selectedItem as Model.RepairResultFilter;
                      Model.DBTable.Repair repair = db.Repairs.Find(RepairResultFilter.Id);

                      MessageBoxResult result1 = View.MessageBox.Show("Видалити ноутбук з бази даних?", "", MessageBoxButton.YesNo, "Так", "Ні", null);
                      switch (result1)
                      {
                          case MessageBoxResult.Yes:
                              repair.Deleted = 1;
                              db.Entry(repair).State = EntityState.Modified;
                              db.SaveChanges();
                              RepResFilt();
                              break;
                          case MessageBoxResult.No:
                              break;
                      }
                  }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}