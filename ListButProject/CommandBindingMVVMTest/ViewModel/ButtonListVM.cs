﻿using CommandBindingMVVMTest.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CommandBindingMVVMTest.ViewModel
{
    public class ButtonListVM : INotifyPropertyChanged
    {
        public ButtonListVM()
        {
            OnCollectionChange();                    // Default buttons from English
        }

        ICommand onButtonClickCommand;
        public ICommand OnButtonClickCommand
        {
            get { return onButtonClickCommand ?? (onButtonClickCommand = new RelayCommand(ButtonClick)); }
        }
        private List<CategoryItem> categorybuttonList = new List<CategoryItem>();
        public List<CategoryItem> CategoryButtonList
        {
            get
            {
                return categorybuttonList;
            }
            set
            {
                categorybuttonList = value;
                RaisePropertyChanged("CategoryButtonList");
            }
        }

        private void ButtonClick(object button)
        {
            Button clickedbutton = button as Button;

            if (clickedbutton != null)
            {
                // Here we can check (clickedbutton.Tag) value with static string properties of ButtonNames class..

                string msg = string.Format("You Pressed : {0} button", clickedbutton.Tag);                
                MessageBox.Show(msg);
            }
        }

        private void OnCollectionChange()
        {
            CategoryItem item = new CategoryItem();
            item.ButtonContent = ButtonNames.SaveButton;
            item.ButtonTag = ButtonNames.SaveButton;
            item.Name = Basket.SaveText; 

            CategoryButtonList = new List<CategoryItem>();           // Intialize the button list

            CategoryButtonList.Add(item);
        }       
    

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }

    public class CategoryItem
    {
        public string ButtonContent { get; set; }
        public string ButtonTag { get; set; }
        public string Name { get; set; }
    }



    
}
