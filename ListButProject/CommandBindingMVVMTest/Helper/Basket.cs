﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandBindingMVVMTest.Helper
{
    public class Basket
    {
        public static string SaveText = "SaveText";
        public static string OpenText = "OpenText";
        public static string CloseText = "CloseText";
        public static string ThisText = "ThisText";
    }
}
