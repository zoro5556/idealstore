﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandBindingMVVMTest.Helper
{
    public class ButtonNames
    {
        public static string SaveButton = "Save";
        public static string OpenButton = "Open";
        public static string CloseButton = "Close";
        public static string ThisButton = "this";
    }


}
